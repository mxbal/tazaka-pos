<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">

                <input type="hidden" name="kategori" value="<?= $kategori->id ?>">

                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama_sub" id="nama" class="form-control" value="<?= isset($subkategori) ? $subkategori->nama : set_value('nama_sub') ?>">

                    <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>