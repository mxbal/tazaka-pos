<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" name="username" id="username" class="form-control" value="<?= isset($user) ? $user->username : set_value('username') ?>">

                    <small class="text-danger"><?= form_error('username') ?></small>
                </div>

                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" id="nama" class="form-control" value="<?= isset($user) ? $user->nama : set_value('nama') ?>">

                    <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control" value="<?= set_value('password') ?>">

                    <small class="text-danger"><?= form_error('password') ?></small>
                </div>

                <div class="form-group">
                    <label for="level">Level</label>
                    <select name="level" id="level" class="form-control">
                        <option disabled selected>-- Pilih Level --</option>
                        <option <?= isset($user) ? $user->level == 'Admin' ? 'selected' : '' : ''  ?> value="Admin">Admin</option>
                        <option <?= isset($user) ? $user->level == 'Staff' ? 'selected' : '' : ''  ?> value="Staff">Staff</option>
                        <option <?= isset($user) ? $user->level == 'User' ? 'selected' : '' : ''  ?> value="User">User</option>
                    </select>

                    <small class="text-danger"><?= form_error('level') ?></small>
                </div>

                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" name="image" id="image" class="form-control" value="<?= set_value('image') ?>">

                    <small class="text-danger"><?= form_error('image') ?></small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>