<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data User</h4>
            <a href="<?= base_url('user/create') ?>" class="btn btn-sm btn-primary mb-3">Tambah User</a>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Image</th>
                            <th>Username</th>
                            <th>Nama</th>
                            <th>Level</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($users as $user) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><img src="<?= base_url('uploads/users/' . $user->image) ?>" alt="<?= $user->image ?>" class="rounded-circle" width="50"></td>
                                <td><?= $user->username ?></td>
                                <td><?= $user->nama ?></td>
                                <td><?= $user->level ?></td>
                                <td>
                                    <a href="<?= base_url('user/edit/' . $user->id) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button type="button" data-url="<?= base_url('user/delete/' . $user->id) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>