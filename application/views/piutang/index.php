<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data Piutang</h4>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Invoice</th>
                            <th>Kasir</th>
                            <th>Tagihan</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($piutang as $ptng) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= date('d/m/Y H:i:s', strtotime($ptng->tanggal)) ?></td>
                                <td><?= $ptng->invoice ?></td>
                                <td><?= $this->db->get_where('users', ['id' => $ptng->user_id])->row()->nama ?></td>
                                <td>Rp. <?= number_format($ptng->total, 0, ',', '.') ?></td>
                                <td><?= $ptng->status ?></td>
                                <td>
                                    <a href="<?= base_url('piutang/edit/' . $ptng->piutang) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <!-- <button type="button" data-url="<?= base_url('piutang/delete/' . $ptng->id) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button> -->
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>