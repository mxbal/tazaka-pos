<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Laporan Piutang</h4>

            <form action="" class="row form-date">
                <div class="form-group col-md-4">
                    <input type="date" name="from" id="from" class="form-control" value="<?= $this->input->get('from') ?>">
                </div>
                <div class="form-group col-md-4">
                    <input type="date" name="to" id="to" class="form-control" value="<?= $this->input->get('to') ?>">
                </div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="<?= base_url('piutang/export') ?>?from=<?= $this->input->get('from') ?>&to=<?= $this->input->get('to') ?>" class="btn btn-success"><i class="fas fa-file-excel"></i> Export</a>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Invoice</th>
                            <th>Tagihan</th>
                            <th>Status</th>
                            <th>Kasir</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($piutang as $ptng) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= date('d/m/Y H:i:s', strtotime($ptng->tanggal)) ?></td>
                                <td><?= $ptng->invoice ?></td>
                                <td>Rp. <?= number_format($ptng->total, 0, ',', '.') ?></td>
                                <td><?= $ptng->status ?></td>
                                <td><?= $this->db->get_where('users', ['id' => $ptng->user_id])->row()->nama ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>