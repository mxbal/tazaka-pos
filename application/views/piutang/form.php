<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">

                <div class="form-group row">
                    <div class="col-md-4">
                        <label for="total_tagihan">Total Tagihan</label>
                        <h2 class="text-dark"><b>Rp. <?= number_format($piutang->total, 0, ',', '.') ?></b></h2>
                        <input type="hidden" name="total-tagihan" id="total-tagihan" value="<?= $piutang->total ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4 mb-3">
                        <label for="tanggal_tempo">Tanggal Jatuh Tempo</label>
                        <input type="date" name="tanggal_tempo" id="tanggal_tempo" class="form-control" value="<?= isset($piutang) ? $piutang->jatuh_tempo : set_value('tanggal_tempo') ?>">

                        <small class="text-danger"><?= form_error('tanggal_tempo') ?></small>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="tanggal_bayar">Tanggal Bayar</label>
                        <input type="date" name="tanggal_bayar" id="tanggal_bayar" class="form-control" value="<?= isset($piutang) ? $piutang->tgl_bayar : date('Y-m-d') ?>">

                        <small class="text-danger"><?= form_error('tanggal_bayar') ?></small>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="status-tagihan">Status</label>
                        <select name="status" id="status-tagihan" class="form-control">
                            <option <?= isset($piutang) ? $piutang->status == 'Lunas' ? 'selected' : '' : '' ?> value="Lunas">Lunas</option>
                            <option <?= isset($piutang) ? $piutang->status == 'Belum Lunas' ? 'selected' : '' : '' ?> value="Belum Lunas">Belum Lunas</option>
                        </select>

                        <small class="text-danger"><?= form_error('status') ?></small>
                    </div>


                    <div class="col-md-6 mb-3">
                        <label for="bayar-tagihan">Bayar</label>
                        <input type="text" name="bayar" id="bayar-tagihan" class="form-control" value="<?= isset($piutang) ? number_format($piutang->bayar, 0, ',', '.') : set_value('bayar') ?>">

                        <small class="text-danger"><?= form_error('bayar') ?></small>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="kembali-tagihan">Kembali</label>
                        <input type="text" name="kembali" id="kembali-tagihan" class="form-control" value="<?= isset($piutang) ? number_format($piutang->kembali, 0, ',', '.') : set_value('kembali') ?>">

                        <small class="text-danger"><?= form_error('kembali') ?></small>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>