<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets') ?>/images/favicon.png">
    <title>POS - <?= $title ?></title>
    <link href="<?= base_url('assets') ?>/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="<?= base_url('assets') ?>/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="<?= base_url('assets') ?>/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="<?= base_url('assets') ?>/css/style.min.css" rel="stylesheet">
    <link href="<?= base_url('assets') ?>/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <div class="main-wrapper">

        <div class="card">
            <div class="card-body">
                <h4 class="text-dark text-center mb-3"><b><?= $sub ?></b></h4>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="data-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIK</th>
                                <th>Kode Member</th>
                                <th>Nama</th>
                                <th>Total Belanja</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($topmember as $top) : ?>
                                <tr>
                                    <td>
                                        <?php if ($no == 1) : ?>
                                            <sup><i class="fas fa-crown text-warning"></i></sup>
                                        <?php endif ?>
                                        <?php if ($no == 2) : ?>
                                            <sup><i class="fas fa-trophy text-warning"></i></sup>
                                        <?php endif ?>
                                        <?php if ($no == 3) : ?>
                                            <sup><i class="fas fa-medal text-warning"></i></sup>
                                        <?php endif ?>
                                        <?= $no++ ?>
                                    </td>
                                    <td><?= $top->nik ?></td>
                                    <td><?= $top->kode_member ?></td>
                                    <td><?= $top->nama ?></td>
                                    <td>Rp. <?= number_format($top->total_belanja, 0, ',', '.');
                                            ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <script src="<?= base_url('assets') ?>/libs/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url('assets') ?>/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?= base_url('assets') ?>/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url('assets') ?>/js/app-style-switcher.js"></script>
    <script src="<?= base_url('assets') ?>/js/feather.min.js"></script>
    <script src="<?= base_url('assets') ?>/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?= base_url('assets') ?>/js/sidebarmenu.js"></script>
    <script src="<?= base_url('assets') ?>/js/custom.min.js"></script>
    <script src="<?= base_url('assets') ?>/extra-libs/c3/d3.min.js"></script>
    <script src="<?= base_url('assets') ?>/extra-libs/c3/c3.min.js"></script>
    <script src="<?= base_url('assets') ?>/libs/chartist/dist/chartist.min.js"></script>
    <script src="<?= base_url('assets') ?>/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="<?= base_url('assets') ?>/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?= base_url('assets') ?>/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?= base_url('assets') ?>/js/pages/dashboards/dashboard1.min.js"></script>
    <script src="<?= base_url('assets') ?>/extra-libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.18/dist/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>

</body>

</html>