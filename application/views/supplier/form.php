<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" id="nama" class="form-control" value="<?= isset($supplier) ? $supplier->nama : set_value('nama') ?>">

                    <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea name="alamat" id="alamat" rows="4" class="form-control">
                    <?= isset($supplier) ? $supplier->alamat : set_value('alamat') ?>
                    </textarea>

                    <small class="text-danger"><?= form_error('alamat') ?></small>
                </div>

                <div class="form-group">
                    <label for="telp">Telp</label>
                    <input type="text" name="telp" id="telp" class="form-control" value="<?= isset($supplier) ? $supplier->telp : set_value('telp') ?>">

                    <small class="text-danger"><?= form_error('telp') ?></small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>