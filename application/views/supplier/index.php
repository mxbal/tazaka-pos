<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data Supplier</h4>
            <a href="<?= base_url('supplier/create') ?>" class="btn btn-sm btn-primary mb-3">Tambah Supplier</a>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Telp</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($supplier as $supp) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $supp->nama ?></td>
                                <td><?= $supp->alamat ?></td>
                                <td><?= $supp->telp ?></td>
                                <td>
                                    <a href="<?= base_url('supplier/edit/' . $supp->slug) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button type="button" data-url="<?= base_url('supplier/delete/' . $supp->slug) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>