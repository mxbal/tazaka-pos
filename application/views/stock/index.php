<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data Stock Barang</h4>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Product</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($stock as $stk) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $stk->nama_product ?></td>
                                <td><?= $stk->jumlah ?></td>
                                <td><?= $stk->satuan ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>