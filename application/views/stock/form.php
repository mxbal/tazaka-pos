<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <div class="col-md-4">
                        <label for="supplier">Supplier</label>
                        <select name="supplier" id="supplier" class="form-control">
                            <option disabled selected>-- Pilih Supplier --</option>
                            <?php foreach ($supplier as $sup) : ?>
                                <option <?= isset($stock) ? $stock->supplier_id == $sup->id ? 'selected' : '' : set_value('supplier') ?> value="<?= $sup->id ?>"><?= $sup->nama ?></option>
                            <?php endforeach; ?>
                        </select>
                        <small class="text-danger"><?= form_error('supplier') ?></small>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="no_faktur">No Faktur</label>
                            <input type="text" name="no_faktur" id="no_faktur" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="tgl">Tanggal Pembelian</label>
                            <input type="date" name="tgl" id="tgl" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="stock-product">Product</label>
                    <select name="stock-product" id="stock-product" class="form-control">
                        <option disabled selected>-- Pilih Product --</option>
                        <?php foreach ($product as $prod) : ?>
                            <option <?= isset($stock) ? $stock->product_id == $prod->id ? 'selected' : '' : set_value('stock-product') ?> value="<?= isset($stock) ? $prod->id : $prod->slug ?>"><?= $prod->nama ?></option>
                        <?php endforeach; ?>
                    </select>
                    <small class="text-danger"><?= form_error('product') ?></small>
                </div>

                <?php if (!isset($stock)) : ?>
                    <label for="prod">List Product</label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Kode Product</th>
                                <th>Nama Product</th>
                                <th>Jumlah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="list-product">

                        </tbody>
                    </table>
                <?php else : ?>
                    <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <input type="number" name="jumlah" id="jumlah" class="form-control" value="<?= $stock->jumlah ?>">
                        <small class="text-danger"><?= form_error('supplier') ?></small>
                    </div>
                <?php endif; ?>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>