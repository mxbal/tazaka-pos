<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Kategori <?= $kategori->nama ?></h4>
            <a href="<?= base_url('subkategori/create/' . $kategori->slug) ?>" class="btn btn-sm btn-primary mb-3">Tambah Sub Kategori</a>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Sub Kategori</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($subs as $sub) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $sub->nama_sub ?></td>
                                <td>
                                    <a href="<?= base_url('subkategori/edit/' . $sub->slug) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button type="button" data-url="<?= base_url('subkategori/delete/' . $sub->slug) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>