<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data Kategori</h4>
            <a href="<?= base_url('kategori/create') ?>" class="btn btn-sm btn-primary mb-3">Tambah Kategori</a>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kategori</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($kategori as $kat) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $kat->nama ?></td>
                                <td>
                                    <a href="<?= base_url('kategori/' . $kat->slug) ?>" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i></a>
                                    <a href="<?= base_url('kategori/edit/' . $kat->slug) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button type="button" data-url="<?= base_url('kategori/delete/' . $kat->slug) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>