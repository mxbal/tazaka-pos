<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Detail Transaksi</h4>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="invoice">No Invoice</label>
                    <h4 class="text-dark"><b><?= $transaksi->invoice ?></b></h4>
                </div>

                <div class="col-md-3">
                    <label for="tanggal">Tanggal</label>
                    <h4 class="text-dark"><b><?= date('d/m/Y H:i:s', strtotime($transaksi->tanggal)) ?></b></h4>
                </div>

                <div class="col-md-3">
                    <label for="kasir">Kasir</label>
                    <h4 class="text-dark"><b><?= $this->db->get_where('users', ['id' => $transaksi->user_id])->row()->nama ?></b></h4>
                </div>

                <div class="col-md-3">
                    <label for="member">Member</label>
                    <h4 class="text-dark"><b><?= $this->db->get_where('member', ['id' => $transaksi->member_id])->row()->nama ?? '-' ?></b></h4>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="total">Total</label>
                    <h4 class="text-dark"><b>Rp. <?= number_format($transaksi->total, 0, ',', '.') ?></b></h4>
                </div>

                <div class="col-md-3">
                    <label for="discount">Discount</label>
                    <?php if ($transaksi->jenis_diskon != 'Persen') : ?>
                        <h4 class="text-dark"><b>Rp. <?= number_format($transaksi->discount, 0, ',', '.') ?></b></h4>
                    <?php else : ?>
                        <h4 class="text-dark"><b> <?= $transaksi->discount ?>%</b></h4>
                    <?php endif; ?>
                </div>

                <div class="col-md-3">
                    <label for="bayar">Bayar</label>
                    <h4 class="text-dark"><b>Rp. <?= number_format($transaksi->bayar, 0, ',', '.') ?></b></h4>
                </div>

                <div class="col-md-3">
                    <label for="kembali">Kembali</label>
                    <h4 class="text-dark"><b>Rp. <?= number_format($transaksi->kembali, 0, ',', '.') ?></b></h4>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="jenis">Jenis Pembayaran</label>
                    <h4 class="text-dark"><b><?= $transaksi->jenis_pembayaran ?></b></h4>
                </div>

                <!-- <div class="col-md-3">
                    <label for="bayar">Bayar</label>
                    <h4 class="text-dark"><b> <?= number_format($transaksi->bayar, 0, ',', '.') ?></b></h4>
                </div> -->
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <label for="product">List Product</label>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th>Nama Product</th>
                                <th class="text-center">Qty</th>
                                <th>Harga</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($detail as $prod) : ?>
                                <tr>
                                    <td class="text-center"><?= $no++ ?></td>
                                    <td><?= $this->db->get_where('product', ['id' => $prod->product_id])->row()->nama ?></td>
                                    <td class="text-center"><?= $prod->qty ?></td>
                                    <td>Rp. <?= number_format($this->db->get_where('product', ['id' => $prod->product_id])->row()->harga_jual, 2, ',', '.') ?></td>
                                    <td>Rp. <?= number_format($this->db->get_where('product', ['id' => $prod->product_id])->row()->harga_jual * $prod->qty, 2, ',', '.') ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="form-group">
                <a href="<?= base_url('transaksi/print/' . $transaksi->invoice) ?>" class="btn btn-success"><i class="fas fa-print"></i> Print</a>
            </div>
        </div>
    </div>
</div>