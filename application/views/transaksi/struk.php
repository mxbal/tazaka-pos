<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $transaction->invoice ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css">

    <style>
        .container {
            font-size: 10pt !important;
            color: black;
            margin-left: 10px !important;
        }

        .table thead tr th {
            border-left: none;
            border-right: none;
        }

        .table tbody tr td {
            border-top: none;
            border-left: none;
            border-right: none;
        }

        .table tfoot tr th {
            border: none;
        }

        @media print {
            .container {
                width: 75mm !important;
            }
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12 d-flex justify-content-between ">
                <div class="logo">
                    <img src="<?= base_url('uploads/setting/' . $setting->image) ?>" alt="" width="100">
                </div>
                <div class="title text-center">
                    <h5><?= $setting->nama ?></h5>
                    <p><?= $setting->alamat ?></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table style="border: none;" width="100%">
                    <tr>
                        <th class="text-left"><?= date('d/m/Y H:i:s', strtotime($transaction->tanggal)) ?></th>
                        <th class="text-end"><?= $transaction->invoice ?></th>
                    </tr>
                    <tr>
                        <th class="text-left"><?= $this->db->get_where('users', ['id' => $transaction->user_id])->row()->nama ?></th>
                        <th class="text-end"><?= $this->db->get_where('member', ['id' => $transaction->member_id])->row()->nama ?? '-' ?></th>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row mb-5">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama Product</th>
                            <th class="text-center">Qty</th>
                            <th>Harga</th>
                            <th class="text-end">Sub Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($order as $prod) : ?>
                            <tr>
                                <td><?= $this->db->get_where('product', ['id' => $prod->product_id])->row()->nama ?></td>
                                <td class="text-center"><?= $prod->qty ?></td>
                                <td><?= number_format($this->db->get_where('product', ['id' => $prod->product_id])->row()->harga_jual, 2, ',', '.') ?></td>
                                <td class="text-end"><?= number_format($this->db->get_where('product', ['id' => $prod->product_id])->row()->harga_jual * $prod->qty, 2, ',', '.') ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3">Jenis Pembayaran</th>
                            <th>
                                <div class="text-end">
                                    <span><?= $transaction->jenis_pembayaran ?></span>
                                </div>
                            </th>
                        </tr>
                        <?php if ($transaction->jenis_pembayaran == 'Tunai') : ?>
                            <tr>
                                <th colspan="3">Total</th>
                                <th>
                                    <div class="d-flex justify-content-between">
                                        <span>Rp. </span>
                                        <span><?= number_format($transaction->total, 2, ',', '.') ?></span>
                                    </div>
                                </th>
                            </tr>
                        <?php else : ?>
                            <tr>
                                <th colspan="3">Total Tagihan</th>
                                <th>
                                    <div class="d-flex justify-content-between">
                                        <span>Rp. </span>
                                        <span><?= number_format($transaction->total, 2, ',', '.') ?></span>
                                    </div>
                                </th>
                            </tr>
                        <?php endif; ?>

                        <?php if ($transaction->status == 'Lunas') : ?>
                            <tr>
                                <th colspan="3">Discount</th>
                                <th>
                                    <div class="<?= $transaction->jenis_diskon == 'Nominal' || $transaction->jenis_diskon == '-' ? 'd-flex justify-content-between' : 'text-end' ?> ">
                                        <?php if ($transaction->jenis_diskon == 'Nominal' || $transaction->jenis_diskon == '-') : ?>
                                            <span>Rp. </span>
                                            <span><?= number_format($transaction->discount, 2, ',', '.') ?></span>
                                        <?php else : ?>
                                            <span><?= $transaction->discount ?>%</span>
                                        <?php endif; ?>
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="3">Bayar</th>
                                <th>
                                    <div class="d-flex justify-content-between">
                                        <span>Rp. </span>
                                        <span><?= number_format($transaction->bayar, 2, ',', '.') ?></span>
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="3">Kembali</th>
                                <th>
                                    <div class="d-flex justify-content-between">
                                        <span>Rp. </span>
                                        <span><?= number_format($transaction->kembali, 2, ',', '.') ?></span>
                                    </div>
                                </th>
                            </tr>
                        <?php endif; ?>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <p><?= $setting->ucapan ?></p>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"></script>

    <script>
        window.onload(print())
    </script>
</body>

</html>