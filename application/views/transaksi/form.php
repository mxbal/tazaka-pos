<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data" id="form-transaksi">
                <input type="hidden" name="inv" value="<?= $trx->invoice ?>">
                <div class="form-group row">
                    <div class="col-md-4 mb-3">
                        <label for="total">Total Bayar</label>
                        <h2 class="text-dark"><b class="total-bayar">Rp. 0</b></h2>
                        <input type="hidden" name="total_bayar" id="total-trx" value="">
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="total-transaksi">Total Transaksi</label>
                        <h2 class="text-secondary"><b class="total-transaksi">Rp. 0</b></h2>
                        <input type="hidden" name="total_transaksi" id="total-transaksi" value="">
                    </div>

                    <div class="col-md-4 ui-widget">
                        <label for="product">Product</label>
                        <input type="text" name="trx-product" id="trx-product" class="form-control">

                        <small class="text-danger"><?= form_error('trx-product') ?></small>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="member">Member</label>
                        <input type="text" name="trx-member" id="trx-member" class="form-control">

                        <input type="hidden" name="member" value="0" id="member">
                        <small class="text-danger"><?= form_error('member') ?></small>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="jenis-pembayaran">Jenis Pembayaran</label>
                        <select name="jenis_pembayaran" id="jenis-pembayaran" class="form-control">
                            <option <?= isset($trx) ? $trx->jenis_pembayaran == 'Tunai' ? 'selected' : '' : '' ?> value="Tunai">Tunai</option>
                            <option <?= isset($trx) ? $trx->jenis_pembayaran == 'Piutang' ? 'selected' : '' : '' ?> value="Piutang">Piutang</option>
                        </select>

                        <small class="text-danger"><?= form_error('jenis_pembayaran') ?></small>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="jenis-diskon">Type Diskon</label>
                        <select name="jenis_diskon" id="jenis-diskon" class="form-control">
                            <option <?= isset($trx) ? $trx->jenis_diskon == 'Nominal' ? 'selected' : '' : '' ?> value="Nominal">Nominal</option>
                            <option <?= isset($trx) ? $trx->jenis_diskon == 'Persen' ? 'selected' : '' : '' ?> value="Persen">Persen</option>
                        </select>

                        <small class="text-danger"><?= form_error('jenis_diskon') ?></small>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="discount">Discount <span class="text-secondary mark-disc"><?= isset($trx) ? $trx->jenis_diskon == 'Nominal' ? '(Rp)' : '(%)' : '' ?></span></label>
                        <input type="text" name="discount" id="discount" <?= isset($trx) ? $trx->jenis_diskon == '-' ? 'disabled' : '' : '' ?> class="form-control" value="<?= isset($trx) ? $trx->jenis_diskon == 'Nominal' ? number_format($trx->discount, 0, ',', '.') : $trx->discount : '0' ?>">

                        <small class="text-danger"><?= form_error('discount') ?></small>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="bayar">Bayar</label>
                        <input type="text" name="bayar" id="bayar" class="form-control" value="<?= isset($trx) ? number_format($trx->bayar, 0, ',', '.') : '0' ?>">

                        <small class="text-danger"><?= form_error('bayar') ?></small>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="kembali">Kembali</label>
                        <input type="text" name="kembali" id="kembali" class="form-control" value="<?= isset($trx) ? number_format($trx->kembali, 0, ',', '.') : '0' ?>">

                        <small class="text-danger"><?= form_error('kembali') ?></small>
                    </div>

                    <div class="col-md-4 mb-3 pembeli d-none">
                        <label for="nama_pembeli">Nama Pembeli</label>
                        <input type="text" name="nama_pembeli" id="nama_pembeli" class="form-control" value="">

                        <small class="text-danger"><?= form_error('nama_pembeli') ?></small>
                    </div>

                    <?php if ($title == 'Edit Transaksi' && isset($trx) && $trx->jenis_pembayaran == 'Piutang') : ?>
                        <div class="col-md-4 status mb-3">
                            <label for="status">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option <?= isset($trx) ? $trx->status == 'Lunas' ? 'selected' : '' : '' ?> value="Lunas">Lunas</option>
                                <option <?= isset($trx) ? $trx->status == 'Belum Lunas' ? 'selected' : '' : '' ?> value="Belum Lunas">Belum Lunas</option>
                            </select>

                            <small class="text-danger"><?= form_error('status') ?></small>
                        </div>

                        <div class="col-md-4 tgl-bayar mb-3">
                            <label for="tgl_bayar">Tanggal Bayar</label>
                            <input type="date" name="tgl_bayar" id="tgl_bayar" class="form-control" value="<?= date('Y-m-d') ?>">

                            <small class="text-danger"><?= form_error('bayar') ?></small>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if (!isset($transaksi)) : ?>
                    <label for="">List Product</label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Kode Product</th>
                                <th>Nama Product</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="list-trx-product">

                        </tbody>
                    </table>
                <?php else : ?>
                    <div class="form-group">
                        <label for="qty">qty</label>
                        <input type="number" name="qty" id="qty" class="form-control" value="<?= $transaksi->qty ?>">
                        <small class="text-danger"><?= form_error('supplier') ?></small>
                    </div>
                <?php endif; ?>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary btn-transaksi">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>