<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data Transaksi</h4>

            <form action="" class="row form-date">
                <div class="form-group col-md-4">
                    <label for="from">From</label>
                    <input type="date" name="from" id="from" class="form-control" value="<?= $this->input->get('from') ?>">
                </div>
                <div class="form-group col-md-4">
                    <label for="to">To</label>
                    <input type="date" name="to" id="to" class="form-control" value="<?= $this->input->get('to') ?>">
                </div>
            </form>

            <div class="table-responsive">
                <a href="<?= base_url('transaksi/create') ?>" class="btn btn-sm btn-primary mb-3">Tambah Transaksi</a>
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Invoice</th>
                            <th>Kasir</th>
                            <th>Jenis Pembayaran</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($transaksi as $trx) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= date('d/m/Y H:i:s', strtotime($trx->tanggal)) ?></td>
                                <td><a href="<?= base_url('transaksi/show/' . $trx->invoice) ?>"><?= $trx->invoice ?></a></td>
                                <td><?= $this->db->get_where('users', ['id' => $trx->user_id])->row()->nama ?></td>
                                <td><?= $trx->jenis_pembayaran ?></td>
                                <td><span class="<?= $trx->status == 'Belum Lunas' ? 'text-danger' : '' ?>"><?= $trx->status ?></span></td>
                                <td>
                                    <a href="<?= base_url('transaksi/print/' . $trx->invoice) ?>" class="btn btn-sm btn-info"><i class="fas fa-print"></i></a>
                                    <a href="<?= base_url('transaksi/edit/' . $trx->invoice) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <!-- <button type="button" data-url="<?= base_url('transaksi/delete/' . $trx->invoice) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button> -->
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>