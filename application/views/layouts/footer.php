<footer class="footer text-center text-muted">
    All Rights Reserved by Adminmart. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
</footer>

</div>

</div>
<script src="<?= base_url('assets') ?>/libs/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url('assets') ?>/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="<?= base_url('assets') ?>/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= base_url('assets') ?>/js/app-style-switcher.js"></script>
<script src="<?= base_url('assets') ?>/js/feather.min.js"></script>
<script src="<?= base_url('assets') ?>/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
<script src="<?= base_url('assets') ?>/js/sidebarmenu.js"></script>
<script src="<?= base_url('assets') ?>/js/custom.min.js"></script>
<script src="<?= base_url('assets') ?>/extra-libs/c3/d3.min.js"></script>
<script src="<?= base_url('assets') ?>/extra-libs/c3/c3.min.js"></script>
<script src="<?= base_url('assets') ?>/libs/chartist/dist/chartist.min.js"></script>
<script src="<?= base_url('assets') ?>/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
<script src="<?= base_url('assets') ?>/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
<script src="<?= base_url('assets') ?>/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?= base_url('assets') ?>/js/pages/dashboards/dashboard1.min.js"></script>
<script src="<?= base_url('assets') ?>/extra-libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.18/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script>
    $("#data-table").DataTable();

    $("#data-table").on('click', '.btn-delete', function() {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Anda akan menghapus data ini secara permanen!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: $(this).attr('data-url'),
                    type: 'DELETE',
                    success: function(response) {
                        Swal.fire(
                            'Terhapus!',
                            'Data berhasil dihapus.',
                            'success'
                        )

                        setTimeout(function() {
                            window.location.reload(true)
                        }, 2000)
                    }
                })

            }
        })
    })
</script>

<script>
    function rupiah(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    function rupiahFormat(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    function formatRupiah(val) {
        val = val.replace(/[^0-9\.]/g, '');

        if (val != "") {
            valArr = val.split('.');
            valArr[0] = (parseInt(valArr[0], 10)).toLocaleString();
            val = valArr.join('.');
        }

        return this.value = val;
    }
</script>

<?php if ($this->uri->segment(1) == 'pembelian') : ?>
    <script>
        let kode_product = $("#kode_product").val();

        if (kode_product == '') {
            $.ajax({
                url: '<?= base_url("product/get-kode") ?>',
                type: 'GET',
                success: function(response) {
                    let kode = JSON.parse(response)
                    $("#kode_product").val(kode.kode)
                }
            })
        } else {
            $("#kode_product").val(kode_product)
        }

        $("#stock-product-pembelian").on('change', function() {
            let slug = $(this).val();
            $.ajax({
                url: '<?= base_url("product/show/") ?>' + slug,
                type: 'GET',
                success: function(response) {
                    let product = JSON.parse(response)
                    $(".list-pembelian").append(`<tr>
                                    <td>` + product.kode_product + `</td>
                                    <input type="hidden" name="product[]" value="` + product.id + `">
                                    <td>` + product.nama + `</td>
                                    <td>
                                    <input type="number" name="jumlah[]" data-id="` + product.id + `" id="jml-` + product.id + `" value="1" class="form-control jumlah">
                                    </td>
                                    <td><input type="text" name="harga[]" data-id="` + product.id + `" value="0" class="form-control harga" id="hrg-` + product.id + `"></td>
                                    <td><span class="total" id="total-` + product.id + `"></span></td>
                                    <td><button type="button" data-id="` + product.id + `" class="btn btn-sm btn-danger btn-remove"><i class="fas fa-times"></i></button></td>
                                </tr>`);
                }
            })
        })

        $(".list-pembelian").on('change', '.harga', function() {
            let id = $(this).attr('data-id');
            let harga = $(this).val();
            let jml = $("#jml-" + id).val();
            let totalHide = parseInt($("#total-pembelian-hid").val());
            let subTotal = parseInt(harga) * parseInt(jml);

            <?php if (isset($pembelian)) : ?>
                let oldHrg = $("#old-hrg-" + id).val();
                let oldJml = $("#old-jml-" + id).val();
                let oldtotal = parseInt($("#old-total-pembelian").val())
                let oldsub = 0;
                if (jml != oldJml) {
                    oldsub = parseInt(oldJml) * parseInt(oldHrg);
                }

                $("#total-" + id).empty().append("Rp. " + rupiah(subTotal))
                $("#total-pembelian").empty().append("Rp. " + rupiah(oldtotal - oldsub + subTotal))
                $("#total-pembelian-hid").val(oldtotal - oldsub + subTotal)
            <?php else : ?>
                $("#total-" + id).empty().append("Rp. " + rupiah(subTotal))
                $("#total-pembelian").empty().append("Rp. " + rupiah(totalHide + subTotal))
                $("#total-pembelian-hid").val(totalHide + subTotal)
            <?php endif; ?>
        })

        $(".list-pembelian").on('change', '.jumlah', function() {
            let id = $(this).attr('data-id');
            let jml = $(this).val();
            let harga = $("#hrg-" + id).val();
            let totalHide = $("#total-pembelian-hid").val();
            let subTotal = parseInt(harga) * parseInt(jml);

            <?php if (isset($pembelian)) : ?>
                let oldJml = $("#old-jml-" + id).val();
                let oldsub = parseInt(harga) * parseInt(oldJml);

                $("#total-" + id).empty().append("Rp. " + rupiah(subTotal))
                $("#total-pembelian").empty().append("Rp. " + rupiah(totalHide - oldsub + subTotal))
                $("#total-pembelian-hid").val(totalHide - oldsub + subTotal)

            <?php else : ?>
                let allJml = $(".jumlah").val()
                $.each(allJml, function(i, data) {
                    console.log(i)
                })

                $("#total-" + id).empty().append("Rp. " + rupiah(subTotal))
                $("#total-pembelian").empty().append("Rp. " + rupiah(totalHide + subTotal))
                $("#total-pembelian-hid").val(totalHide + subTotal)
            <?php endif; ?>
        })

        $(".list-pembelian").on('click', '.btn-remove', function() {
            let id = $(this).attr('data-id');
            let harga = $("#hrg-" + id).val();
            let jml = $("#jml-" + id).val();
            let total = parseInt(harga) * parseInt(jml)
            let totalHide = parseInt($("#total-pembelian-hid").val());

            $("#total-pembelian").empty().append("Rp. " + rupiah(totalHide - total))
            $("#total-pembelian-hid").val(totalHide - total)

            $(this).parent().parent().remove()
        })
    </script>
<?php endif; ?>

<?php if ($this->uri->segment(1) == 'transaksi') : ?>
    <script>
        let totalBayar = 0;

        $(document).ready(function() {
            // Transaksi
            let trxId = '<?= $trx->id ?>';
            let totalSemua = 0;
            let totalTrx = 0;

            function getListTransaksi() {
                $.ajax({
                    url: '<?= base_url('detail-transaksi/') ?>' + trxId,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function(response) {
                        $.each(response.detail, function(i, data) {
                            $(".list-trx-product").append(`<tr>
                                            <td>` + data.kode_product + `</td>
                                            <input type="hidden" name="product[]" value="` + data.product + `">
                                            <input type="hidden" value="` + data.harga_jual + `" class="price-` + data.product + `">
                                            <input type="hidden" value="` + data.total + `" class="total-trx-` + data.product + `">
                                            <td>` + data.nama + `</td>
                                            <td><input type="number" name="qty[]" data-id="` + data.product + `" value="` + data.qty + `" class="form-control trx-qty"></td>
                                            <td><span class="price">Rp. ` + rupiah(data.harga_jual) + `</span></td>
                                            <td><span class="total-` + data.product + `">Rp. ` + rupiah(data.total) + `</span></td>
                                            <td><button type="button" class="btn btn-sm btn-danger btn-remove" data-id="` + data.product + `"><i class="fas fa-times"></i></button></td>
                                        </tr>`);
                        });

                        $(".total-bayar").empty().append('Rp. ' + rupiah(response.disc));
                        $("#total-trx").val(response.disc);

                        $(".total-transaksi").empty().append('Rp. ' + rupiah(response.all));
                        $("#total-transaksi").val(response.all);

                        totalSemua = response.all;
                        totalTrx = response.disc;
                        kembali($("#jenis-diskon").val())
                    }
                })
            }

            getListTransaksi()

            // $("#trx-product").focus();

            // $("#trx-product").autocomplete({
            //     minLength: 3,
            //     source: function(req, resp) {
            //         $.ajax({
            //             type: "POST",
            //             url: "<?= base_url('product/get') ?>",
            //             data: 'search=' + req.term,
            //             success: function(d) {
            //                 console.log(d)
            //                 resp(d);
            //             }
            //         });
            //     },
            //     select: function(e, ui) {
            //         $("#trx-product").val(ui.item.label);
            //         $.ajax({
            //             url: '<?= base_url("detail-transaksi/store") ?>',
            //             method: 'POST',
            //             type: 'POST',
            //             data: {
            //                 trx: trxId,
            //                 product: ui.item.value,
            //                 qty: 1
            //             },
            //             success: function(response) {
            //                 $(".list-trx-product").empty()
            //                 getListTransaksi()
            //                 $("#trx-product").val("");
            //                 $("#trx-product").focus();
            //             }
            //         })

            //         return false;
            //     }
            // });

            $('#form-transaksi').on('keyup keypress', '#trx-product #trx-member', function(e) {
                var keyCode = e.keyCode || e.which;

                if (keyCode === 13) {
                    e.preventDefault();

                    return false;
                }
            })

            $("#trx-product").on('keypress', function(e) {
                let product = $(this).val();
                var keyCode = e.keyCode || e.which;

                if (keyCode === 13) {
                    e.preventDefault();

                    $.ajax({
                        url: '<?= base_url("detail-transaksi/store") ?>',
                        method: 'POST',
                        type: 'POST',
                        data: {
                            trx: trxId,
                            product: product,
                            qty: 1
                        },
                        success: function(response) {
                            $(".list-trx-product").empty()
                            getListTransaksi()
                            $("#trx-product").val("");
                        }
                    })

                    return false;
                }

            })

            $("#trx-member").on('keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            })

            $(".list-trx-product").on('click', '.btn-remove', function() {
                let product = $(this).attr('data-id');

                $.ajax({
                    url: '<?= base_url("detail-transaksi/delete") ?>',
                    type: 'GET',
                    data: {
                        trx: trxId,
                        product: product,
                    },
                    success: function(response) {
                        $(".list-trx-product").empty()
                        getListTransaksi()
                    }
                })

            })

            $(".list-trx-product").on('change', '.trx-qty', function() {
                let id = $(this).attr('data-id');

                $.ajax({
                    url: '<?= base_url("detail-transaksi/update") ?>',
                    type: 'POST',
                    data: {
                        trx: trxId,
                        product: id,
                        qty: $(this).val()
                    },
                    success: function(response) {
                        $(".list-trx-product").empty()
                        getListTransaksi()
                    }
                })
            })

            // $("#trx-member").autocomplete({
            //     minLength: 3,
            //     source: function(req, resp) {
            //         $.ajax({
            //             type: "POST",
            //             url: "<?= base_url('member/get') ?>",
            //             data: 'search=' + req.term,
            //             success: function(d) {
            //                 console.log(d)
            //                 resp(d);
            //             }
            //         });
            //     },
            //     select: function(e, ui) {
            //         $("#trx-member").val(ui.item.label);
            //         $("#member").val(ui.item.value)

            //         return false;
            //     }
            // });

            $("#jenis-pembayaran").on('change', function() {
                let val = $(this).val();

                if (val == 'Piutang') {
                    $("#bayar").attr('readonly', 'readonly')
                    $("#kembali").attr('readonly', 'readonly')
                    $("#kembali").val(0)
                    $("#jenis-diskon").attr('readonly', 'readonly')
                    $("#discount").attr('readonly', 'readonly')
                    $(".pembeli").removeClass('d-none')
                }

                if (val == 'Tunai') {
                    $("#bayar").removeAttr('readonly')
                    $("#kembali").removeAttr('readonly')
                    $("#jenis-diskon").removeAttr('readonly')
                    $("#discount").removeAttr('readonly')
                    $(".pembeli").addClass('d-none')
                }
            })

            $("#jenis-diskon").on('change', function() {
                let value = $(this).val();

                if (value == 'Nominal') {
                    $("#discount").val(0)
                    $(".mark-disc").empty().append("(Rp.)")
                } else {
                    $(".mark-disc").empty().append("(%)")
                    $("#discount").val(0)
                }

                $.ajax({
                    url: '<?= base_url('transaksi/counted') ?>',
                    type: 'GET',
                    data: {
                        trx: trxId,
                        field: 'jenis_diskon',
                        val: $(this).val()
                    },
                    success: function(response) {
                        console.log(response)
                    }
                })

                $.ajax({
                    url: '<?= base_url('transaksi/counted') ?>',
                    type: 'GET',
                    data: {
                        trx: trxId,
                        field: 'discount',
                        val: 0
                    },
                    success: function(response) {
                        console.log(response)
                    }
                })

                $.ajax({
                    url: '<?= base_url('transaksi/counted') ?>',
                    type: 'GET',
                    data: {
                        trx: trxId,
                        field: 'kembali',
                        val: 0
                    },
                    success: function(response) {
                        console.log(response)
                    }
                })

                kembali(value)
            })

            $("#discount").on('change', function() {
                let value = $(this).val();
                let jenis_diskon = $("#jenis-diskon").val()
                let total = $("#total-trx").val();
                let discount = 0;
                let totaltrx = parseInt($("#total-transaksi").val());

                if (jenis_diskon == 'Nominal') {
                    $(this).val(rupiah($(this).val()));

                    let disc = parseInt($(this).val().replace('.', ''));
                    let bayar = parseInt($("#bayar").val().replace('.', ''));
                    discount = parseInt(totaltrx - parseInt($(this).val().replace('.', '')));

                    $.ajax({
                        url: '<?= base_url('transaksi/counted') ?>',
                        type: 'GET',
                        data: {
                            trx: trxId,
                            field: 'discount',
                            val: parseInt($(this).val().replace('.', ''))
                        },
                        success: function(response) {
                            console.log(response)
                        }
                    })

                    $(".total-bayar").empty().append('Rp. ' + rupiah(discount));
                    $("#total-trx").val(discount);
                }

                if (jenis_diskon == 'Persen') {
                    let disk = parseInt(totaltrx * value) / 100;
                    discount = totaltrx - disk;

                    $.ajax({
                        url: '<?= base_url('transaksi/counted') ?>',
                        type: 'GET',
                        data: {
                            trx: trxId,
                            field: 'discount',
                            val: parseInt($(this).val())
                        },
                        success: function(response) {
                            console.log(response)
                        }
                    })

                    $(".total-bayar").empty().append('Rp. ' + rupiah(discount));
                    $("#total-trx").val(discount);
                }

                kembali(jenis_diskon)

            })

            $("#bayar").on('change', function() {
                $(this).val(rupiah($(this).val()));

                $.ajax({
                    url: '<?= base_url('transaksi/counted') ?>',
                    type: 'GET',
                    data: {
                        trx: trxId,
                        field: 'bayar',
                        val: parseInt($(this).val().replace('.', ''))
                    },
                    success: function(response) {
                        console.log(response)
                    }
                })


                $.ajax({
                    url: '<?= base_url('transaksi/counted') ?>',
                    type: 'GET',
                    data: {
                        trx: trxId,
                        field: 'total',
                        val: parseInt($("#total-transaksi").val())
                    },
                    success: function(response) {
                        console.log(response)
                    }
                })




                kembali($("#jenis-diskon").val())
            })

            function kembali(jenis) {
                let totaltrx = $("#total-transaksi").val()
                let bayar = $("#bayar").val().replace('.', '')
                let kembali = 0;
                let discount = $("#discount").val()

                if (jenis == 'Nominal') {
                    let kembali = parseInt(bayar - (totaltrx - discount.replace('.', '')));
                    $("#kembali").val()
                    $("#kembali").val(rupiah(kembali))

                    $.ajax({
                        url: '<?= base_url('transaksi/counted') ?>',
                        type: 'GET',
                        data: {
                            trx: trxId,
                            field: 'kembali',
                            val: kembali
                        },
                        success: function(response) {
                            console.log(response)
                        }
                    })
                }

                if (jenis == 'Persen') {
                    let disc = parseInt(totaltrx * discount) / 100;
                    let kembali = parseInt(bayar - (totaltrx - disc));
                    $("#kembali").val()
                    $("#kembali").val(rupiah(kembali))

                    $.ajax({
                        url: '<?= base_url('transaksi/counted') ?>',
                        type: 'GET',
                        data: {
                            trx: trxId,
                            field: 'kembali',
                            val: kembali
                        },
                        success: function(response) {
                            console.log(response)
                        }
                    })
                }

            }


            setTimeout(function() {
                $(".btn-transaksi").on('click', function() {
                    window.open('<?= base_url("transaksi/print/" . $trx->invoice) ?>', '_blank');
                })
            }, 2000)

            $("#to").on('change', function() {
                $(".form-date").submit()
            })
        });
    </script>
<?php endif; ?>

<?php if ($this->uri->segment(1) == 'piutang' && $this->uri->segment(2) == 'edit') : ?>
    <script>
        function rupiah(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }

        $(document).ready(function() {
            $("#status-tagihan").on('change', function() {
                let val = $(this).val();
                let tagihan = $("#total-tagihan").val();

                if (val == 'Lunas') {
                    $("#bayar-tagihan").val(rupiah(tagihan))
                    $("#kembali-tagihan").val(rupiah(0))
                } else {
                    bayar()
                }
            })

            function bayar() {
                $("#bayar-tagihan").on('change', function() {
                    let bayar = $(this).val();
                    let kembali = bayar - $("#total-tagihan").val();
                    console.log(bayar)
                    $(this).val(rupiah(bayar))
                    $("#kembali-tagihan").val()
                    $("#kembali-tagihan").val(rupiah(kembali))
                })
            }

            bayar()

        })
    </script>
<?php endif; ?>

<?php if ($this->uri->segment(1) == 'member') : ?>
    <script>
        $(document).ready(function() {
            $("#rfid").on('keypress', function(e) {
                $('#form-member').on('keyup keypress', function(e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        e.preventDefault();
                        return false;
                    }
                });
            })
        })
    </script>
<?php endif; ?>

<?php if ($this->uri->segment(1) == 'product') : ?>
    <script>
        $(document).ready(function() {
            $("#kode_product").on('keypress', function(e) {
                $('#form-product').on('keyup keypress', function(e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        e.preventDefault();
                        return false;
                    }
                });
            })
        })
    </script>
<?php endif; ?>

<?php if ($this->session->flashdata('success')) : ?>
    <script>
        Swal.fire(
            'Selamat!',
            '<?= $this->session->flashdata("success") ?>',
            'success'
        )
    </script>
<?php endif; ?>

<?php if ($this->session->flashdata('error')) : ?>
    <script>
        Swal.fire(
            'Selamat!',
            '<?= $this->session->flashdata("error") ?>',
            'error'
        )
    </script>
<?php endif; ?>
</body>

</html>