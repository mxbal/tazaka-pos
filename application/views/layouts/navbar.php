<aside class="left-sidebar" data-sidebarbg="skin6">
    <div class="scroll-sidebar" data-sidebarbg="skin6">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <?php if ($this->session->userdata('level') == 'Admin') : ?>
                    <li class="sidebar-item">
                        <a class="sidebar-link sidebar-link" href="<?= base_url('dashboard') ?>" aria-expanded="false"><i data-feather="home" class="feather-icon">
                            </i><span class="hide-menu">Dashboard</span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                            <i data-feather="file-text" class="feather-icon"></i>
                            <span class="hide-menu">Data Master</span>
                        </a>

                        <ul aria-expanded="false" class="collapse  first-level base-level-line">
                            <li class="sidebar-item">
                                <a href="<?= base_url('user') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Data User</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="<?= base_url('member') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Data Member</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="<?= base_url('kategori') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Data Kategori</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="<?= base_url('supplier') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Data Supplier</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                            <i class="fas fa-box"></i>
                            <span class="hide-menu">Data Product</span>
                        </a>

                        <ul aria-expanded="false" class="collapse first-level base-level-line">
                            <li class="sidebar-item">
                                <a href="<?= base_url('satuan') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Data Satuan</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="<?= base_url('product') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Data Product</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="<?= base_url('stock') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Data Stock Product</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="<?= base_url('pembelian') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Data Pembelian</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="<?= base_url('lost-product') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Data Lost Product</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                <?php endif; ?>

                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="<?= base_url('transaksi') ?>" aria-expanded="false">
                        <i class="fas fa-donate"></i>
                        <span class="hide-menu">Transaksi</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="<?= base_url('piutang') ?>" aria-expanded="false">
                        <i class="fas fa-credit-card"></i>
                        <span class="hide-menu">Piutang</span>
                    </a>
                </li>

                <?php if ($this->session->userdata('level') == 'Admin') : ?>
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                            <i class="fas fa-file"></i>
                            <span class="hide-menu">Laporan</span>
                        </a>

                        <ul aria-expanded="false" class="collapse first-level base-level-line">
                            <li class="sidebar-item">
                                <a href="<?= base_url('laporan/transaksi') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Laporan Transaksi</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="<?= base_url('laporan/piutang') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Laporan Piutang</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="<?= base_url('laporan/penjualan') ?>" class="sidebar-link">
                                    <span class="hide-menu"> Laporan Penjualan</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link sidebar-link" href="<?= base_url('setting') ?>" aria-expanded="false">
                            <i class="fa fa-cog"></i>
                            <span class="hide-menu">Setting</span>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
        </nav>
    </div>
</aside>