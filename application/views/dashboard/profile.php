<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" id="nama" class="form-control" value="<?= isset($user) ? $user->nama : set_value('nama') ?>">

                    <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="text" name="password" id="password" class="form-control" value="<?= isset($user) ? '' : set_value('password') ?>">

                    <small class="text-danger"><?= form_error('password') ?></small>
                </div>

                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" name="image" id="image" class="form-control" value="">

                    <small class="text-danger"><?= form_error('image') ?></small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>