<div class="container-fluid">
    <div class="card-group">
        <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-dark mb-1 font-weight-medium"><?= $product ?></h2>
                        </div>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Total Product</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="user-plus"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium"><?= $transaksi ?></h2>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Total Transaksi
                        </h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="dollar-sign"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-dark mb-1 font-weight-medium"><?= $piutang ?></h2>
                            <span class="badge bg-danger font-12 text-white font-weight-medium badge-pill ml-2 d-md-none d-lg-block">Belum Lunas</span>
                        </div>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Total Piutang</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="file-plus"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <h2 class="text-dark mb-1 font-weight-medium"><?= $member ?></h2>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Total Member</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="globe"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <h4 class="text-dark mb-3"><b>Top 10 Member Bulan <?= date('F') ?></b></h4>
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>Kode Member</th>
                            <th>Nama</th>
                            <th>Total Belanja</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($topten as $top) : ?>
                            <tr>
                                <td>
                                    <?php if ($no == 1) : ?>
                                        <sup><i class="fas fa-crown text-warning"></i></sup>
                                    <?php endif ?>
                                    <?php if ($no == 2) : ?>
                                        <sup><i class="fas fa-trophy text-warning"></i></sup>
                                    <?php endif ?>
                                    <?php if ($no == 3) : ?>
                                        <sup><i class="fas fa-medal text-warning"></i></sup>
                                    <?php endif ?>
                                    <?= $no++ ?>
                                </td>
                                <td><?= $top->nik ?></td>
                                <td><?= $top->kode_member ?></td>
                                <td><?= $top->nama ?></td>
                                <td>Rp. <?= number_format($top->total_belanja, 0, ',', '.');
                                        ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>