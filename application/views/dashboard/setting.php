<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" id="nama" class="form-control" value="<?= isset($setting) ? $setting->nama : set_value('nama') ?>">

                    <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea name="alamat" id="alamat" cols="30" rows="4" class="form-control"><?= isset($setting) ? $setting->alamat : set_value('alamat') ?></textarea>

                    <small class="text-danger"><?= form_error('alamat') ?></small>
                </div>

                <div class="form-group">
                    <label for="ucapan">Ucapan</label>
                    <input type="text" name="ucapan" id="ucapan" class="form-control" value="<?= isset($setting) ? $setting->ucapan : set_value('ucapan') ?>">

                    <small class="text-danger"><?= form_error('ucapan') ?></small>
                </div>

                <div class="form-group">
                    <label for="image">Logo</label>
                    <input type="file" name="image" id="image" class="form-control" value="">

                    <small class="text-danger"><?= form_error('image') ?></small>
                </div>

                <div class="form-group">
                    <label for="bg">Background</label>
                    <input type="file" name="bg" id="bg" class="form-control" value="">

                    <small class="text-danger"><?= form_error('bg') ?></small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Sinkron Database</h4>
            <form action="<?= base_url('sync') ?>" method="post">

                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="hostname">Hostname</label>
                        <input type="text" name="hostname" id="hostname" class="form-control" value="<?= $sync->host ?>">
                    </div>
                    <div class="col-md-6">
                        <label for="username">Username</label>
                        <input type="text" name="username" id="username" class="form-control" value="<?= $sync->username ?>">
                    </div>
                    <div class="col-md-6">
                        <label for="password">Password</label>
                        <input type="text" name="password" id="password" class="form-control" value="<?= $sync->password ?>">
                    </div>
                    <div class="col-md-6">
                        <label for="database">Database</label>
                        <input type="text" name="database" id="database" class="form-control" value="<?= $sync->db ?>">
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success" onclick="return confirm('Sinkronkan database?')"><i class="fas fa-sync"></i> Sinkron</button>
                </div>
            </form>
        </div>
    </div>
</div>