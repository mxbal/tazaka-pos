<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data Product</h4>
            <a href="<?= base_url('product/create') ?>" class="btn btn-sm btn-primary mb-3">Tambah Product</a>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Product</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Harga Beli</th>
                            <th>Harga Jual</th>
                            <th>Satuan</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($product as $prod) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $prod->kode_product ?></td>
                                <td><?= $prod->nama ?></td>
                                <td><?= $this->db->get_where('kategori', ['id' => $prod->kategori_id])->row()->nama ?></td>
                                <td>Rp. <?= number_format($prod->harga_beli, 2, ',', '.') ?></td>
                                <td>Rp. <?= number_format($prod->harga_jual, 2, ',', '.') ?></td>
                                <td><?= $this->db->get_where('satuan', ['id' => $prod->satuan_id])->row()->nama ?></td>
                                <td>
                                    <a href="<?= base_url('product/edit/' . $prod->slug) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button type="button" data-url="<?= base_url('product/delete/' . $prod->slug) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>