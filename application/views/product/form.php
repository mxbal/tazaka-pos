<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data" id="form-product">

                <div class="form-group">
                    <label for="kode_product">Kode Product</label>
                    <input type="text" name="kode_product" id="kode_product" autofocus="autofocus" class="form-control" value="<?= isset($product) ? $product->kode_product : set_value('kode_product') ?>">

                    <small class="text-danger"><?= form_error('kode_product') ?></small>
                </div>

                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" id="nama" class="form-control" value="<?= isset($product) ? $product->nama : set_value('nama') ?>">

                    <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                    <label for="kategori">Kategori</label>
                    <select name="kategori" id="kategori" class="form-control">
                        <option disabled selected>-- Pilih Kategori --</option>
                        <?php foreach ($kategori as $kat) : ?>
                            <option <?= isset($product) ? $product->kategori_id == $kat->id ? 'selected' : '' : '' ?> value="<?= $kat->id ?>"><?= $kat->nama ?></option>
                        <?php endforeach; ?>
                    </select>

                    <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                    <label for="satuan">Satuan</label>
                    <select name="satuan" id="satuan" class="form-control">
                        <option disabled selected>-- Pilih satuan --</option>
                        <?php foreach ($satuan as $sat) : ?>
                            <option <?= isset($product) ? $product->satuan_id == $sat->id ? 'selected' : '' : '' ?> value="<?= $sat->id ?>"><?= $sat->nama ?></option>
                        <?php endforeach; ?>
                    </select>

                    <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <!-- <div class="form-group">
                    <label for="harga_beli">Harga Beli</label>
                    <input type="number" name="harga_beli" id="harga_beli" class="form-control" value="<?= isset($product) ? $product->harga_beli : set_value('harga_beli') ?>">

                    <small class="text-danger"><?= form_error('harga_beli') ?></small>
                </div> -->

                <div class="form-group">
                    <label for="harga_jual">Harga Jual</label>
                    <input type="number" name="harga_jual" id="harga_jual" class="form-control" value="<?= isset($product) ? $product->harga_jual : set_value('harga_jual') ?>">

                    <small class="text-danger"><?= form_error('harga_jual') ?></small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>