<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Laporan Penjualan</h4>

            <form action="" class="row form-date">
                <div class="form-group col-md-4">
                    <input type="date" name="from" id="from" class="form-control" value="<?= $this->input->get('from') ?>">
                </div>
                <div class="form-group col-md-4">
                    <input type="date" name="to" id="to" class="form-control" value="<?= $this->input->get('to') ?>">
                </div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="<?= base_url('penjualan/export') ?>?from=<?= $this->input->get('from') ?>&to=<?= $this->input->get('to') ?>" class="btn btn-success"><i class="fas fa-file-excel"></i> Export</a>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Product</th>
                            <th>Nama Product</th>
                            <th>Stock</th>
                            <th>Terjual</th>
                            <th>Tersedia</th>
                            <th>Harga Beli</th>
                            <th>Harga Jual</th>
                            <th>Total Penjualan</th>
                            <th>Keuntungan</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        $beli = 0;
                        $jual = 0;
                        $total = 0;
                        $untung = 0;
                        foreach ($penjualan as $penj) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $penj->kode_product ?></td>
                                <td><?= $penj->nama ?></td>
                                <td>
                                    <?= $this->db->query("SELECT SUM(detail_pembelian.jumlah) as jml FROM detail_pembelian WHERE product_id = '$penj->id' ")->row()->jml ?> <?= $penj->satuan ?>
                                </td>
                                <td>
                                    <?= $this->db->query("SELECT SUM(detail_transaksi.qty) as terjual FROM detail_transaksi WHERE product_id = '$penj->id' ")->row()->terjual ?> <?= $penj->satuan ?>
                                </td>
                                <td><?= $penj->stok ?> <?= $penj->satuan ?></td>
                                <td>Rp. <?= number_format($penj->harga_beli, 2, ',', '.') ?></td>
                                <td>Rp. <?= number_format($penj->harga_jual, 2, ',', '.') ?></td>
                                <td>Rp. <?= number_format($penj->harga_jual * $this->db->query("SELECT SUM(detail_transaksi.qty) as terjual FROM detail_transaksi WHERE product_id = '$penj->id' ")->row()->terjual, 2, ',', '.') ?></td>
                                <td>Rp. <?= number_format(($penj->harga_jual - $penj->harga_beli) * $this->db->query("SELECT SUM(detail_transaksi.qty) as terjual FROM detail_transaksi WHERE product_id = '$penj->id' ")->row()->terjual, 2, ',', '.') ?></td>
                                <?php
                                $beli += $penj->harga_beli;
                                $jual += $penj->harga_jual;
                                $total += $penj->harga_jual * $this->db->query("SELECT SUM(detail_transaksi.qty) as terjual FROM detail_transaksi WHERE product_id = '$penj->id' ")->row()->terjual;
                                $untung += ($penj->harga_jual - $penj->harga_beli) * $this->db->query("SELECT SUM(detail_transaksi.qty) as terjual FROM detail_transaksi WHERE product_id = '$penj->id' ")->row()->terjual;
                                ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th colspan="6">Total All : </th>
                            <th>Rp. <?= number_format($beli, 2, ',', '.') ?></th>
                            <th>Rp. <?= number_format($jual, 2, ',', '.') ?></th>
                            <th>Rp. <?= number_format($total, 2, ',', '.') ?></th>
                            <th>Rp. <?= number_format($untung, 2, ',', '.') ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>