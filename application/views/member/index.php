<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data Member</h4>
            <a href="<?= base_url('member/create') ?>" class="btn btn-sm btn-primary mb-3">Tambah Member</a>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Image</th>
                            <th>Kode Member</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($members as $mem) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><img src="<?= base_url('uploads/member/' . $mem->foto) ?>" alt="<?= $mem->foto ?>" class="rounded-circle" width="50"></td>
                                <td><?= $mem->kode_member ?></td>
                                <td><?= $mem->nama ?></td>
                                <td><?= $mem->alamat ?></td>
                                <td>
                                    <a href="<?= base_url('member/edit/' . $mem->kode_member) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button type="button" data-url="<?= base_url('member/delete/' . $mem->kode_member) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>