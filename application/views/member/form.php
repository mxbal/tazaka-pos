<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data" id="form-member">
                <div class="form-group">
                    <label for="rfid">RFID</label>
                    <input type="text" name="rfid" id="rfid" class="form-control" value="<?= isset($member) ? $member->rfid : set_value('rfid') ?>" autofocus>

                    <small class="text-danger"><?= form_error('rfid') ?></small>
                </div>

                <div class="form-group">
                    <label for="nik">NIK</label>
                    <input type="number" name="nik" id="nik" class="form-control" value="<?= isset($member) ? $member->nik : set_value('nik') ?>">

                    <small class="text-danger"><?= form_error('nik') ?></small>
                </div>

                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" id="nama" class="form-control" value="<?= isset($member) ? $member->nama : set_value('nama') ?>">

                    <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea name="alamat" id="alamat" cols="30" rows="4" class="form-control"><?= isset($member) ? $member->alamat : set_value('alamat') ?></textarea>

                    <small class="text-danger"><?= form_error('alamat') ?></small>
                </div>

                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" name="image" id="image" class="form-control" value="">

                    <small class="text-danger"><?= form_error('image') ?></small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>