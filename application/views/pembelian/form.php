<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="total-pembelian">Total</label>
                            <?php if (isset($pembelian)) { ?>
                                <h3 class="text-dark"><b id="total-pembelian">Rp. <?= number_format($pembelian->total, 0, ',', '.') ?></b></h3>
                                <input type="hidden" name="total-pembelian" id="total-pembelian-hid" class="form-control" value="<?= $pembelian->total ?>">
                                <input type="hidden" name="total-pembelian" id="old-total-pembelian" class="form-control" value="<?= $pembelian->total ?>">
                            <?php } else {
                            ?>
                                <h3 class="text-dark"><b id="total-pembelian">Rp. 0</b></h3>
                                <input type="hidden" name="total-pembelian" id="total-pembelian-hid" class="form-control" value="0">
                            <?php } ?>
                        </div>
                    </div> -->

                    <div class="col-md-4">
                        <label for="supplier">Supplier</label>
                        <select name="supplier" id="supplier" class="form-control">
                            <option disabled selected>-- Pilih Supplier --</option>
                            <?php foreach ($supplier as $sup) : ?>
                                <option <?= isset($pembelian) ? $pembelian->supplier_id == $sup->id ? 'selected' : '' : set_value('supplier') ?> value="<?= $sup->id ?>"><?= $sup->nama ?></option>
                            <?php endforeach; ?>
                        </select>
                        <small class="text-danger"><?= form_error('supplier') ?></small>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="tgl">Tanggal Pembelian</label>
                            <input type="date" name="tgl" id="tgl" class="form-control" value="<?= isset($pembelian) ? $pembelian->tanggal_pembelian : '' ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="stock-product">Product</label>
                    <select name="stock-product" id="stock-product-pembelian" class="form-control">
                        <option disabled selected>-- Pilih Product --</option>
                        <?php foreach ($product as $prod) : ?>
                            <option value="<?= $prod->slug ?>"><?= $prod->nama ?></option>
                        <?php endforeach; ?>
                    </select>
                    <small class="text-danger"><?= form_error('product') ?></small>
                </div>

                <label for="prod">List Product</label>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Kode Product</th>
                            <th>Nama Product</th>
                            <th>Jumlah</th>
                            <th>Harga Beli</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="list-pembelian">
                        <?php if (!isset($pembelian)) : ?>

                        <?php else : ?>
                            <?php foreach ($detail as $det) : ?>
                                <tr>
                                    <?php if (isset($pembelian)) : ?>
                                        <input type="hidden" name="product[]" value="<?= $det->product_id ?>">
                                    <?php endif; ?>
                                    <td><?= $this->db->get_where('product', ['id' => $det->product_id])->row()->kode_product ?></td>
                                    <td><?= $this->db->get_where('product', ['id' => $det->product_id])->row()->nama ?></td>
                                    <td>
                                        <input type="number" name="jumlah[]" data-id="<?= $det->product_id ?>" id="jml-<?= $det->product_id ?>" value="<?= $det->jumlah ?>" class="form-control jumlah">
                                        <?php if (isset($pembelian)) : ?>
                                            <input type="hidden" name="" id="old-jml-<?= $det->product_id ?>" value="<?= $det->jumlah ?>">
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <input type="number" name="harga[]" data-id="<?= $det->product_id ?>" value="<?= $det->harga ?>" class="form-control harga" id="hrg-<?= $det->product_id ?>">
                                        <?php if (isset($pembelian)) : ?>
                                            <input type="hidden" name="" id="old-hrg-<?= $det->product_id ?>" value="<?= $det->harga ?>">
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <span class="total" id="total-<?= $det->product_id ?>">Rp. <?= number_format($det->total, 0, ',', '.') ?></span>
                                    </td>
                                    <td><button type="button" data-id="<?= $det->product_id ?>" class="btn btn-sm btn-danger btn-remove"><i class="fas fa-times"></i></button></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>