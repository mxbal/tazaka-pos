<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data Pembelian</h4>
            <a href="<?= base_url('pembelian/create') ?>" class="btn btn-sm btn-primary mb-3">Tambah Pembelian</a>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>No Pembelian</th>
                            <th>Supplier</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($pembelian as $pmb) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= date('d/m/Y', strtotime($pmb->tanggal_pembelian)) ?></td>
                                <td><?= $pmb->no_pembelian ?></td>
                                <td><?= $pmb->supplier ?></td>
                                <td>Rp. <?= number_format($pmb->total, 0, ',', '.') ?></td>
                                <td>
                                    <a href="<?= base_url('pembelian/edit/' . $pmb->no_pembelian) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button type="button" data-url="<?= base_url('pembelian/delete/' . $pmb->no_pembelian) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>