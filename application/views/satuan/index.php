<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data Satuan</h4>
            <a href="<?= base_url('satuan/create') ?>" class="btn btn-sm btn-primary mb-3">Tambah Satuan</a>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Satuan</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($satuan as $sat) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $sat->nama ?></td>
                                <td>
                                    <a href="<?= base_url('satuan/edit/' . $sat->slug) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button type="button" data-url="<?= base_url('satuan/delete/' . $sat->slug) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>