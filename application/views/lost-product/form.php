<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3"><?= $title ?></h4>

            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="product">Product</label>
                    <select name="product" id="product" class="form-control">
                        <option disabled selected>-- Pilih Product --</option>
                        <?php foreach ($product as $prod) : ?>
                            <option <?= isset($lost) ? $lost->product_id == $prod->id ? 'selected' : '' : '' ?> value="<?= $prod->id ?>"><?= $prod->nama ?></option>
                        <?php endforeach; ?>
                    </select>

                    <small class="text-danger"><?= form_error('product') ?></small>
                </div>

                <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="number" name="jumlah" id="jumlah" class="form-control" value="<?= isset($lost) ? $lost->jumlah : set_value('jumlah') ?>">

                    <small class="text-danger"><?= form_error('jumlah') ?></small>
                </div>

                <div class="form-group">
                    <label for="ket">Ket</label>
                    <select name="ket" id="ket" class="form-control">
                        <option disabled selected>-- Pilih Keterangan --</option>
                        <option <?= isset($lost) ? $lost->ket == 'Expired' ? 'selected' : '' : '' ?> value="Expired">Expired</option>
                        <option <?= isset($lost) ? $lost->ket == 'Rusak' ? 'selected' : '' : '' ?> value="Rusak">Rusak</option>
                    </select>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>