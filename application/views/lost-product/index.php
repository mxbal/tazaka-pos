<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-3">Data Lost Product</h4>
            <a href="<?= base_url('lost-product/create') ?>" class="btn btn-sm btn-primary mb-3">Tambah Lost Product</a>

            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Product</th>
                            <th>Jumlah</th>
                            <th>Ket</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1;
                        foreach ($product as $prod) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= date('d/m/Y H:i', strtotime($prod->created_at)) ?></td>
                                <td><?= $prod->nama ?></td>
                                <td><?= $prod->jumlah ?></td>
                                <td><?= $prod->ket ?></td>
                                <td>
                                    <a href="<?= base_url('lost-product/edit/' . $prod->id) ?>" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button type="button" data-url="<?= base_url('lost-product/delete/' . $prod->id) ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i> </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>