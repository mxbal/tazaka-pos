<?php

function isLogin()
{
    if ($_SESSION['login'] != true) {
        redirect(base_url());
    }
}

function isAdmin()
{
    if ($_SESSION['level'] != 'Admin') {
        redirect($_SERVER['HTTP_REFERER']);
    }
}

function logo()
{
    $conn = mysqli_connect('localhost', 'root', 'root', 'pos');
    $logo = mysqli_query($conn, "SELECT * FROM setting WHERE id = 1")->fetch_assoc()['image'];
    return $logo;
}

function bg()
{
    $conn = mysqli_connect('localhost', 'root', 'root', 'pos');
    $bg = mysqli_query($conn, "SELECT * FROM setting WHERE id = 1")->fetch_assoc()['bg'];
    return $bg;
}

function marketName()
{
    $conn = mysqli_connect('localhost', 'root', 'root', 'pos');
    $nama = mysqli_query($conn, "SELECT * FROM setting WHERE id = 1")->fetch_assoc()['nama'] ?? 'Tazaka';
    return $nama;
}
