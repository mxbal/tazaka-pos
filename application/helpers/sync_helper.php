<?php

function synchronize($host, $username, $password, $db)
{
    $conn = mysqli_connect("localhost", "root", "", "pos");
    $conn_target = mysqli_connect($host, $username, $password, $db);

    $users = mysqli_query($conn, "SELECT * FROM users WHERE is_sync = 0");
    $member = mysqli_query($conn, "SELECT * FROM member WHERE is_sync = 0");
    $kategori = mysqli_query($conn, "SELECT * FROM kategori WHERE is_sync = 0");
    $supplier = mysqli_query($conn, "SELECT * FROM supplier WHERE is_sync = 0");
    $satuan = mysqli_query($conn, "SELECT * FROM satuan WHERE is_sync = 0");
    $stock = mysqli_query($conn, "SELECT * FROM stock WHERE is_sync = 0");
    $pembelian = mysqli_query($conn, "SELECT * FROM pembelian WHERE is_sync = 0");
    $detail_pembelian = mysqli_query($conn, "SELECT * FROM detail_pembelian WHERE is_sync = 0");
    $lost = mysqli_query($conn, "SELECT * FROM lost WHERE is_sync = 0");
    $transaksi = mysqli_query($conn, "SELECT * FROM transaksi WHERE is_sync = 0");
    $detail_transaksi = mysqli_query($conn, "SELECT * FROM detail_transaksi WHERE is_sync = 0");
    $piutang = mysqli_query($conn, "SELECT * FROM piutang WHERE is_sync = 0");
    $setting = mysqli_query($conn, "SELECT * FROM setting WHERE id = 1")->fetch_object();

    foreach ($users as $user) {
        $user = (object) $user;

        mysqli_query($conn_target, "INSERT INTO users VALUES ('$user->id', '$user->username', '$user->nama', '$user->password', '$user->level', '$user->image', 1) ");

        mysqli_query($conn, "UPDATE users SET is_sync = 1 WHERE id = '$user->id'");
    }

    foreach ($member as $mem) {
        $mem = (object) $mem;

        mysqli_query($conn_target, "INSERT INTO member VALUES ('$mem->id', '$mem->kode_member', '$mem->nik', '$mem->nama', '$mem->alamat', '$mem->foto', 1) ");

        mysqli_query($conn, "UPDATE member SET is_sync = 1 WHERE id = '$mem->id'");
    }

    foreach ($kategori as $kat) {
        $kat = (object) $kat;

        mysqli_query($conn_target, "INSERT INTO kategori VALUES ('$kat->id', '$kat->slug', '$kat->nama', 1) ");

        mysqli_query($conn, "UPDATE kategori SET is_sync = 1 WHERE id = '$kat->id'");
    }

    foreach ($supplier as $sup) {
        $sup = (object) $sup;

        mysqli_query($conn_target, "INSERT INTO supplier VALUES ('$sup->id', '$sup->slug', '$sup->nama', '$sup->alamat', '$sup->telp', 1) ");

        mysqli_query($conn, "UPDATE supplier SET is_sync = 1 WHERE id = '$sup->id'");
    }

    foreach ($satuan as $sat) {
        $sat = (object) $sat;

        mysqli_query($conn_target, "INSERT INTO satuan VALUES ('$sat->id', '$sat->nama', '$sat->slug',  1) ");

        mysqli_query($conn, "UPDATE satuan SET is_sync = 1 WHERE id = '$sat->id'");
    }

    foreach ($stock as $stk) {
        $stk = (object) $stk;

        mysqli_query($conn_target, "INSERT INTO stock VALUES ('$stk->id', '$stk->product_id', '$stk->jumlah', 1) ");

        mysqli_query($conn, "UPDATE stock SET is_sync = 1 WHERE id = '$stk->id'");
    }

    foreach ($pembelian as $pmb) {
        $pmb = (object) $pmb;

        mysqli_query($conn_target, "INSERT INTO pembelian VALUES ('$pmb->id', '$pmb->supplier_id', '$pmb->no_pembelian', '$pmb->tanggal_pembelian', '$pmb->total', 1) ");

        mysqli_query($conn, "UPDATE pembelian SET is_sync = 1 WHERE id = '$pmb->id'");
    }

    foreach ($detail_pembelian as $dtpmb) {
        $dtpmb = (object) $dtpmb;

        mysqli_query($conn_target, "INSERT INTO detail_pembelian VALUES ('$dtpmb->id', '$dtpmb->pembelian_id', '$dtpmb->product_id', '$dtpmb->jumlah', '$dtpmb->harga', '$dtpmb->total', 1) ");

        mysqli_query($conn, "UPDATE detail_pembelian SET is_sync = 1 WHERE id = '$dtpmb->id'");
    }

    foreach ($lost as $lst) {
        $lst = (object) $lst;

        mysqli_query($conn_target, "INSERT INTO lost VALUES ('$lst->id', '$lst->product_id', '$lst->jumlah', '$lst->ket', '$lst->created_at', '$lst->updated_at', 1) ");

        mysqli_query($conn, "UPDATE lost SET is_sync = 1 WHERE id = '$lst->id'");
    }

    foreach ($transaksi as $trx) {
        $trx = (object) $trx;

        mysqli_query($conn_target, "INSERT INTO transaksi VALUES ('$trx->id', '$trx->user_id', '$trx->member_id', '$trx->invoice', '$trx->total', '$trx->bayar', '$trx->jenis_diskon', '$trx->discount', '$trx->kembali', '$trx->jenis_pembayaran', '$trx->status', '$trx->is_finish', '$trx->tanggal', 1) ");

        mysqli_query($conn, "UPDATE transaksi SET is_sync = 1 WHERE id = '$trx->id'");
    }

    foreach ($detail_transaksi as $dttrx) {
        $dttrx = (object) $dttrx;

        mysqli_query($conn_target, "INSERT INTO detail_transaksi VALUES ('$dttrx->id', '$dttrx->transaksi_id', '$dttrx->product_id', '$dttrx->qty', 1) ");

        mysqli_query($conn, "UPDATE detail_transaksi SET is_sync = 1 WHERE id = '$dttrx->id'");
    }

    foreach ($piutang as $pit) {
        $pit = (object) $pit;

        mysqli_query($conn_target, "INSERT INTO piutang VALUES ('$pit->id', '$pit->transaksi_id', '$pit->jatuh_tempo', '$pit->tgl_bayar', 1) ");

        mysqli_query($conn, "UPDATE piutang SET is_sync = 1 WHERE id = '$pit->id'");
    }

    $affected = mysqli_query($conn_target, "UPDATE setting SET nama = '$setting->nama', alamat = '$setting->alamat', ucapan = '$setting->ucapan', image = '$setting->image', ");

    return mysqli_affected_rows($conn_target);
}
