<?php

class ListMemberController extends CI_Controller
{
    public function bulanan()
    {
        $month = date('m');
        $data['title'] = "Top Member Bulanan";
        $data['sub'] = "Top 10 Member Bulan " . date('F');

        $data['topmember'] = $this->db->query("SELECT SUM(total - discount) as total_belanja, member.* FROM `transaksi` JOIN member ON transaksi.member_id = member.id WHERE MONTH(tanggal) = '$month' GROUP BY member_id ORDER BY total_belanja DESC LIMIT 10")->result();

        $this->load->view('list-member/index', $data);
    }

    public function tahunan()
    {
        $year = date('Y');
        $data['title'] = "Top Member Tahunan";
        $data['sub'] = "Top 10 Member Tahun " . $year;

        $data['topmember'] = $this->db->query("SELECT SUM(total - discount) as total_belanja, member.* FROM `transaksi` JOIN member ON transaksi.member_id = member.id WHERE YEAR(tanggal) = '$year' GROUP BY member_id ORDER BY total_belanja DESC LIMIT 10")->result();

        $this->load->view('list-member/index', $data);
    }
}
