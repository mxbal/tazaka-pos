<?php

class SatuanController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();

        $this->load->model('Satuan');
    }

    public function index()
    {
        $data['title'] = 'Data Satuan';
        $data['breadcrumb'] = 'Data Satuan';
        $data['url_breadcrumb'] = base_url('satuan');

        $data['satuan'] = $this->Satuan->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('satuan/index');
        $this->load->view('layouts/footer');
    }

    public function create()
    {
        $data['title'] = 'Tambah Satuan';
        $data['breadcrumb'] = 'Tambah Satuan';
        $data['url_breadcrumb'] = base_url('satuan');

        $data['action'] = base_url('satuan/store');

        $this->load->view('layouts/header', $data);
        $this->load->view('satuan/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data = [
                'nama' => $this->input->post('nama', true),
                'slug' => url_title($this->input->post('nama', true), "-", true),
            ];

            $this->Satuan->create($data);

            $this->session->set_flashdata('success', 'Satuan berhasil ditambahkan');
            redirect(base_url('satuan'));
        }
    }

    public function edit($slug)
    {
        $data['title'] = 'Edit Satuan';
        $data['breadcrumb'] = 'Edit Satuan';
        $data['url_breadcrumb'] = base_url('satuan');

        $data['action'] = base_url('satuan/update/' . $slug);
        $data['satuan'] = $this->Satuan->find($slug);

        $this->load->view('layouts/header', $data);
        $this->load->view('satuan/form');
        $this->load->view('layouts/footer');
    }

    public function update($slug)
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($slug);
        } else {
            $data = [
                'nama' => $this->input->post('nama', true),
                'slug' => url_title($this->input->post('nama', true), "-", true),
            ];

            $this->Satuan->update($slug, $data);

            $this->session->set_flashdata('success', 'Satuan berhasil diupdate');
            redirect(base_url('satuan'));
        }
    }

    public function delete($slug)
    {
        $this->Satuan->delete($slug);

        $this->session->set_flashdata('success', 'Satuan berhasil dihapus');
        redirect(base_url('satuan'));
    }
}
