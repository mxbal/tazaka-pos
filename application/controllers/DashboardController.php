<?php

class DashboardController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();
    }

    public function index()
    {
        $month = date('m');

        $data['title'] = 'Dashboard';
        $data['breadcrumb'] = 'Dashboard';
        $data['url_breadcrumb'] = base_url('dashboard');

        $data['product'] = count($this->db->get('product')->result());
        $data['transaksi'] = count($this->db->get_where('transaksi', ['is_finish' => 1])->result());
        $data['piutang'] = count($this->db->query("SELECT * FROM piutang JOIN transaksi WHERE transaksi.status = 'Belum Lunas'")->result());
        $data['member'] = count($this->db->get('member')->result());
        $data['topten'] = $this->db->query("SELECT SUM(total - discount) as total_belanja, member.* FROM `transaksi` JOIN member ON transaksi.member_id = member.id WHERE MONTH(tanggal) = '$month' GROUP BY member_id ORDER BY total_belanja DESC LIMIT 10")->result();

        $this->load->view('layouts/header', $data);
        $this->load->view('dashboard/index');
        $this->load->view('layouts/footer');
    }

    public function setting()
    {
        $data['title'] = 'Setting';
        $data['breadcrumb'] = 'Setting';
        $data['url_breadcrumb'] = base_url('setting');

        $data['setting'] = $this->db->get_where('setting', ['id' => 1])->row();
        $data['sync'] = $this->db->get_where('sync', ['id' => 1])->row();
        $data['action'] = base_url('setting/update');

        $this->load->view('layouts/header', $data);
        $this->load->view('dashboard/setting');
        $this->load->view('layouts/footer');
    }

    public function updatesetting()
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required'
            ],
            [
                'field' => 'ucapan',
                'label' => 'Ucapan',
                'rules' => 'required'
            ],
        ];

        $setting = $this->db->get_where('setting', ['id' => 1])->row();

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->setting();
        } else {
            if ($_FILES['image']['name'] != '') {

                $config['upload_path'] = 'uploads/setting/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtolower($setting->nama) . '_' . $_FILES['image']['name'];

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $image = $this->upload->data();
                    unlink('uploads/setting/' . $setting->image);
                    $filename = $image['file_name'];
                } else {
                    $filename = $setting->image;
                }
            } else {
                $filename = $setting->image;
            }

            if ($_FILES['bg']['name'] != '') {

                $config['upload_path'] = 'uploads/setting/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtolower($setting->nama) . '_' . $_FILES['bg']['name'];

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('bg')) {
                    $bg = $this->upload->data();
                    unlink('uploads/setting/' . $setting->bg);
                    $bgname = $bg['file_name'];
                } else {
                    $bgname = $setting->bg;
                }
            } else {
                $bgname = $setting->bg;
            }

            $data = [
                'nama' => $this->input->post('nama', true),
                'alamat' => $this->input->post('alamat', true),
                'ucapan' => $this->input->post('ucapan', true),
                'image' => $filename,
                'bg' => $bgname,
            ];

            $this->db->update('setting', $data, ['id' => 1]);

            $this->session->set_flashdata('success', 'Setting berhasil diupdate');
            redirect(base_url('setting'));
        }
    }

    public function profile()
    {
        $data['title'] = 'Profile';
        $data['breadcrumb'] = 'Profile';
        $data['url_breadcrumb'] = base_url('profile');

        $data['user'] = $this->db->get_where('users', ['id' => $this->session->userdata('id')])->row();
        $data['action'] = base_url('profile/update');

        $this->load->view('layouts/header', $data);
        $this->load->view('dashboard/profile');
        $this->load->view('layouts/footer');
    }

    public function updateprofile()
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
        ];

        $id = $this->session->userdata('id');
        $user = $this->db->get_where('users', ['id' => $id])->row();

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->profile();
        } else {
            if ($_FILES['image']['name'] != '') {

                $config['upload_path'] = 'uploads/users/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtolower($user->nama) . '_' . $_FILES['image']['name'];

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $image = $this->upload->data();
                    unlink('uploads/user/' . $user->image);
                    $filename = $image['file_name'];
                } else {
                    $filename = $user->image;
                }
            } else {
                $filename = $user->image;
            }

            if ($this->input->post('password') != '') {
                $password = password_hash($this->input->post('password', true), PASSWORD_DEFAULT);
            } else {
                $password = $user->password;
            }

            $data = [
                'nama' => $this->input->post('nama', true),
                'password' => $password,
                'image' => $filename,
            ];

            $this->db->update('users', $data, ['id' => $id]);

            $this->session->set_flashdata('success', 'Profile berhasil diupdate');
            redirect(base_url('profile'));
        }
    }

    public function sync()
    {
        $this->load->helper('sync');

        $hostname = $this->input->post('hostname', true);
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);
        $database = $this->input->post('database', true);

        $data = [
            'host' => $hostname,
            'username' => $username,
            'password' => $password,
            'db' => $database,
        ];

        $this->db->update('sync', $data, ['id' => 1]);

        if (synchronize($hostname, $username, $password, $database) == true) {
            $this->session->set_flashdata('success', 'Sinkron database berhasil');
        } else {
            $this->session->set_flashdata('error', 'Sinkron database gagal');
        }

        redirect(base_url('setting'));
    }
}
