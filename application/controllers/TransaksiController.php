<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

date_default_timezone_set('Asia/Jakarta');

class TransaksiController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();

        $this->load->model('Transaksi');
        $this->load->model('Product');
        $this->load->model('Member');
    }

    public function index()
    {
        $data['title'] = 'Data Transaksi';
        $data['breadcrumb'] = 'Data Transaksi';
        $data['url_breadcrumb'] = base_url('transaksi');

        if ($this->session->userdata('level') != 'Admin') {
            $user = $this->session->userdata('id');
        } else {
            $user = '';
        }

        if ($this->input->get('from', true) && $this->input->get('to', true)) {
            $from = date('Y-m-d H:i:s', strtotime($this->input->get('from', true)));
            $to = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($this->input->get('to', true))));

            $data['transaksi'] = $this->Transaksi->get($from, $to, $user);
        } else {
            $data['transaksi'] = $this->Transaksi->get($user);
        }

        $this->load->view('layouts/header', $data);
        $this->load->view('transaksi/index');
        $this->load->view('layouts/footer');
    }

    public function create()
    {
        $data['title'] = 'Tambah Transaksi';
        $data['breadcrumb'] = 'Tambah Transaksi';
        $data['url_breadcrumb'] = base_url('transaksi');

        $data['action'] = base_url('transaksi/store/');
        $data['product'] = $this->Product->get();
        $data['member'] = $this->Member->get();

        $user = $this->session->userdata('id');
        $last = $this->db->query("SELECT * FROM transaksi WHERE user_id = $user AND is_finish = 0 LIMIT 1")->row();

        if ($last == NULL) {
            $trx = [
                'invoice' => 'INV' . date('YmdHis') . rand(100, 999),
                'user_id' => $this->session->userdata('id'),
            ];

            $this->Transaksi->create($trx);
        }

        $data['trx'] = $this->db->query("SELECT * FROM transaksi WHERE user_id = $user AND is_finish = 0 LIMIT 1")->row();

        $this->load->view('layouts/header', $data);
        $this->load->view('transaksi/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'bayar',
                'label' => 'Bayar',
                'rules' => 'required'
            ],
            [
                'field' => 'kembali',
                'label' => 'Kembalian',
                'rules' => 'required'
            ],
            [
                'field' => 'member',
                'label' => 'Member',
                'rules' => 'required'
            ],
            [
                'field' => 'jenis_pembayaran',
                'label' => 'Jenis Pembayaran',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $inv = $this->input->post('inv', true);
            $trx = $this->Transaksi->find($inv);

            $data = [
                'total' => $this->input->post('total_transaksi', true),
                'member_id' => $this->input->post('member', true),
                'jenis_diskon' => $this->input->post('jenis_diskon', true),
                'jenis_pembayaran' => $this->input->post('jenis_pembayaran', true),
                'discount' => str_replace('.', '', $this->input->post('discount', true)),
                'bayar' => str_replace('.', '', $this->input->post('bayar', true)),
                'kembali' => $this->input->post('jenis_pembayaran', true) == 'Tunai' ?  str_replace('.', '', $this->input->post('kembali', true)) : 0,
                'tanggal' => date('Y-m-d H:i:s'),
                'status' => $this->input->post('jenis_pembayaran', true) == 'Tunai' ? 'Lunas' : 'Belum Lunas',
                'is_finish' => 1
            ];

            if ($this->input->post('jenis_pembayaran', true) == 'Piutang') {
                $jth = date('Y-m-d H:i:s', strtotime('+3 day', strtotime(date('Y-m-d'))));
                $this->db->insert('piutang', ['transaksi_id' => $trx->id, 'jatuh_tempo' => $jth, ['nama_pembeli' => $this->input->post('nama_pembeli', true)]]);
            }

            $this->Transaksi->update($inv, $data);

            redirect(base_url('transaksi/create'));
        }
    }

    public function show($slug)
    {
        $data['title'] = 'Detail Transaksi';
        $data['breadcrumb'] = 'Detail Transaksi';
        $data['url_breadcrumb'] = base_url('transaksi');

        $data['transaksi'] = $this->Transaksi->find($slug);
        $data['detail'] = $this->db->get_where('detail_transaksi', ['transaksi_id' => $data['transaksi']->id])->result();

        $this->load->view('layouts/header', $data);
        $this->load->view('transaksi/show');
        $this->load->view('layouts/footer');
    }

    public function edit($slug)
    {
        $data['title'] = 'Edit Transaksi';
        $data['breadcrumb'] = 'Edit Transaksi';
        $data['url_breadcrumb'] = base_url('transaksi');

        $data['product'] = $this->Product->get();
        $data['member'] = $this->Member->get();
        $data['action'] = base_url('transaksi/update/' . $slug);
        $data['trx'] = $this->Transaksi->find($slug);

        $this->load->view('layouts/header', $data);
        $this->load->view('transaksi/form');
        $this->load->view('layouts/footer');
    }

    public function update($slug)
    {
        $validation = [
            [
                'field' => 'bayar',
                'label' => 'Bayar',
                'rules' => 'required'
            ],
            [
                'field' => 'kembali',
                'label' => 'Kembalian',
                'rules' => 'required'
            ],
            [
                'field' => 'member',
                'label' => 'Member',
                'rules' => 'required'
            ],
            [
                'field' => 'jenis_pembayaran',
                'label' => 'Jenis Pembayaran',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($slug);
        } else {
            $inv = $this->input->post('inv', true);
            $trx = $this->Transaksi->find($inv);

            $data = [
                'total' => $this->input->post('total_transaksi', true),
                'member_id' => $this->input->post('member', true),
                'jenis_diskon' => $this->input->post('jenis_diskon', true),
                'jenis_pembayaran' => $this->input->post('jenis_pembayaran', true),
                'discount' => str_replace('.', '', $this->input->post('discount', true)),
                'bayar' => str_replace('.', '', $this->input->post('bayar', true)),
                'kembali' => $this->input->post('status', true) == 'Lunas' ?  str_replace('.', '', $this->input->post('kembali', true)) : 0,
                'tanggal' => date('Y-m-d H:i:s'),
                'status' => $this->input->post('status', true) ?? $trx->status,
                'is_finish' => 1
            ];

            if ($this->input->post('status', true) == 'Belum Lunas') {
                $jth = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($trx->tanggal)));
                $this->db->update('piutang', ['transaksi_id' => $trx->id, 'jatuh_tempo' => $jth], ['transaksi_id' => $trx->id]);
            }

            if ($this->input->post('status', true) == 'Lunas') {
                $jth = $this->input->post('tgl_bayar', true);
                $this->db->update('piutang', ['transaksi_id' => $trx->id, 'tgl_bayar' => $jth], ['transaksi_id' => $trx->id]);
            }

            $this->Transaksi->update($inv, $data);

            $this->session->set_flashdata('success', 'Transaksi berhasil diupdate');
            redirect(base_url('transaksi'));
        }
    }

    public function delete($slug)
    {
        $this->Transaksi->delete($slug);

        $this->session->set_flashdata('success', 'Transaksi berhasil dihapus');
        redirect(base_url('transaksi'));
    }

    public function counted()
    {
        $trx = $this->input->get('trx', true);
        $field = $this->input->get('field', true);
        $val = $this->input->get('val', true);

        $this->db->update('transaksi', [$field => $val], ['id' => $trx]);

        echo json_encode([
            'message' => 'Success'
        ]);
    }

    public function print($invoice)
    {
        $transaksi = $this->Transaksi->find($invoice);
        $data['transaction'] = $this->db->get_where('transaksi', ['id' => $transaksi->id])->row();
        $data['order'] = $this->db->get_where('detail_transaksi', ['transaksi_id' => $transaksi->id])->result();
        $data['setting'] = $this->db->get_where('setting', ['id' => 1])->row();

        $this->load->view('transaksi/struk', $data);
    }

    public function export()
    {
        if ($this->input->get('from', true) && $this->input->get('to', true)) {
            $from = date('Y-m-d H:i:s', strtotime($this->input->get('from', true)));
            $to = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($this->input->get('to', true))));

            $transaksi = $this->Transaksi->get($from, $to);
        } else {
            $transaksi = $this->Transaksi->get();
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Tanggal');
        $sheet->setCellValue('C1', 'Invoice');
        $sheet->setCellValue('D1', 'Jenis Pembayaran');
        $sheet->setCellValue('E1', 'Jenis Discount');
        $sheet->setCellValue('F1', 'Total');
        $sheet->setCellValue('G1', 'Discount');
        $sheet->setCellValue('H1', 'Bayar');
        $sheet->setCellValue('I1', 'Kembali');
        $sheet->setCellValue('J1', 'Member');
        $sheet->setCellValue('K1', 'Kasir');
        $rows = 2;
        $no = 1;
        foreach ($transaksi as $val) {
            $sheet->setCellValue('A' . $rows, $no++);
            $sheet->setCellValue('B' . $rows, date('Y-m-d H:i:s', strtotime($val->tanggal)));
            $sheet->setCellValue('C' . $rows, $val->invoice);
            $sheet->setCellValue('D' . $rows, $val->jenis_pembayaran);
            $sheet->setCellValue('E' . $rows, $val->jenis_diskon);
            $sheet->setCellValue('F' . $rows, number_format($val->total, 2, '.', '.'));
            $sheet->setCellValue('G' . $rows, $val->jenis_diskon == 'Persen' ? $val->discount . '%' : $val->discount);
            $sheet->setCellValue('H' . $rows, number_format($val->bayar, 2, '.', '.'));
            $sheet->setCellValue('I' . $rows, number_format($val->kembali, 2, '.', '.'));
            $sheet->setCellValue('J' . $rows, $this->db->get_where('member', ['id' => $val->member_id])->row()->nama ?? '-');
            $sheet->setCellValue('K' . $rows, $this->db->get_where('users', ['id' => $val->user_id])->row()->nama);
            $rows++;
        }

        $filename = 'Laporan Transaksi';

        $writer = new Xlsx($spreadsheet);
        header("Content-Type: application/vnd.ms-excel");
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }

    public function laporan()
    {
        $data['title'] = 'Data Transaksi';
        $data['breadcrumb'] = 'Data Transaksi';
        $data['url_breadcrumb'] = base_url('transaksi');

        if ($this->input->get('from', true) && $this->input->get('to', true)) {
            $from = date('Y-m-d H:i:s', strtotime($this->input->get('from', true)));
            $to = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($this->input->get('to', true))));

            $data['transaksi'] = $this->Transaksi->get($from, $to);
        } else {
            $data['transaksi'] = $this->Transaksi->get();
        }


        $this->load->view('layouts/header', $data);
        $this->load->view('transaksi/laporan');
        $this->load->view('layouts/footer');
    }
}
