<?php

class MemberController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();

        $this->load->model('Member');
    }

    public function index()
    {
        $data['title'] = 'Data Member';
        $data['breadcrumb'] = 'Data Member';
        $data['url_breadcrumb'] = base_url('member');

        $data['members'] = $this->Member->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('member/index');
        $this->load->view('layouts/footer');
    }

    public function create()
    {
        $data['title'] = 'Tambah Member';
        $data['breadcrumb'] = 'Tambah Member';
        $data['url_breadcrumb'] = base_url('member');

        $data['action'] = base_url('member/store');

        $this->load->view('layouts/header', $data);
        $this->load->view('member/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'rfid',
                'label' => 'RFID',
                'rules' => 'required'
            ],
            [
                'field' => 'nik',
                'label' => 'NIK',
                'rules' => 'required'
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required'
            ],
        ];


        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            if (!empty($_FILES['image']['name'])) {

                // $kode = 'TZX' . date('ymd') . rand(1000, 9999);
                $kode = $this->input->post('rfid', true);
                $config['upload_path'] = 'uploads/member/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtolower($kode) . '_' . $_FILES['image']['name'];

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $image = $this->upload->data();
                    $filename = $image['file_name'];

                    $data = [
                        'kode_member' => $kode,
                        'nik' => $this->input->post('nik', true),
                        'nama' => $this->input->post('nama', true),
                        'alamat' => $this->input->post('alamat', true),
                        'foto' => $filename,
                    ];

                    $this->Member->create($data);

                    $this->session->set_flashdata('success', 'Member berhasil ditambahkan');
                    redirect(base_url('member'));
                } else {
                    redirect(base_url('member/create'));
                }
            }
        }
    }

    public function edit($kode)
    {
        $data['title'] = 'Edit Member';
        $data['breadcrumb'] = 'Edit Member';
        $data['url_breadcrumb'] = base_url('member');

        $data['action'] = base_url('member/update/' . $kode);
        $data['member'] = $this->Member->find($kode);

        $this->load->view('layouts/header', $data);
        $this->load->view('member/form');
        $this->load->view('layouts/footer');
    }

    public function update($kode)
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required'
            ],
        ];

        $member = $this->Member->find($kode);

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($kode);
        } else {
            if ($_FILES['image']['name'] != '') {

                $config['upload_path'] = 'uploads/member/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtolower($member->kode_member) . '_' . $_FILES['image']['name'];

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $image = $this->upload->data();
                    unlink('uploads/member/' . $member->foto);
                    $filename = $image['file_name'];
                } else {
                    $filename = $member->foto;
                }
            } else {
                $filename = $member->foto;
            }

            $data = [
                'nik' => $this->input->post('nik', true),
                'nama' => $this->input->post('nama', true),
                'alamat' => $this->input->post('alamat', true),
                'foto' => $filename,
            ];

            $this->Member->update($kode, $data);

            $this->session->set_flashdata('success', 'Member berhasil diupdate');
            redirect(base_url('member'));
        }
    }

    public function delete($kode)
    {
        $member = $this->Member->find($kode);
        unlink('uploads/member/' . $member->image);

        $this->Member->delete($kode);

        $this->session->set_flashdata('success', 'Member berhasil dihapus');
        redirect(base_url('member'));
    }

    public function get()
    {
        $search = $this->input->post('search', true);
        $query = $this->db->query("SELECT * FROM member WHERE kode_member LIKE '%$search%' OR nama LIKE '%$search%' ")->result();
        $result = [];

        foreach ($query as $q) {
            $result[] = [
                'label' => $q->kode_member . ' - ' . $q->nama,
                'value' => $q->id
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }
}
