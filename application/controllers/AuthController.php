<?php

class AuthController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        if ($this->session->userdata('login') == true) {
            redirect('dashboard');
        }

        $this->load->view('auth/login');
    }

    public function login()
    {
        if ($this->session->userdata('login') == true) {
            redirect('dashboard');
        }

        $validation = [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $username = $this->input->post('username', true);
            $password = $this->input->post('password', true);

            $user = $this->db->get_where('users', ['username' => $username])->row();
            if ($user) {
                if (password_verify($password, $user->password)) {
                    $this->session->set_userdata([
                        'login' => true,
                        'id' => $user->id,
                        'username' => $user->username,
                        'nama' => $user->nama,
                        'level' => $user->level,
                        'image' => $user->image,
                    ]);

                    if ($user->level == 'Admin') {
                        redirect(base_url('dashboard'));
                    } else {
                        redirect(base_url('transaksi'));
                    }
                } else {
                    $this->session->set_flashdata('error', 'Username atau Password salah');

                    $this->index();
                }
            } else {
                $this->session->set_flashdata('error', 'Username atau password salah');

                $this->index();
            }
        }
    }


    public function logout()
    {
        $this->session->unset_userdata([
            'login', 'id', 'username', 'nama', 'level', 'image',
        ]);
        session_destroy();

        redirect(base_url());
    }
}
