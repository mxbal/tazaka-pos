<?php

class DetailTransaksiController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        $this->load->model('Product');
    }

    public function get($id)
    {
        $all = 0;
        $disc = 0;

        $detail = $this->db->query("SELECT detail_transaksi.*, product.id as product, product.harga_jual, product.kode_product, product.nama, product.harga_jual * detail_transaksi.qty as total FROM detail_transaksi JOIN product ON detail_transaksi.product_id = product.id WHERE transaksi_id = '$id'")->result();

        $trx = $this->db->get_where('transaksi', ['id' => $id])->row();
        foreach ($detail as $det) {
            $all += $det->total;
        }

        if ($trx->jenis_diskon == 'Nominal') {
            $disc = intval($all - $trx->discount);
        }

        if ($trx->jenis_diskon == 'Persen') {
            $count = intval($all * $trx->discount) / 100;
            $disc = $all - $count;
        }

        echo json_encode([
            'all' => $all,
            'trx' => $trx,
            'disc' => $disc,
            'detail' => $detail
        ]);
    }

    public function store()
    {
        $trx = $this->input->post('trx', true);
        $product = $this->db->get_where('product', ['kode_product' => $this->input->post('product', true)])->row();
        $productId = $product->id;
        $qty = $this->input->post('qty', true);
        $message = '';

        $detail = $this->db->query("SELECT * FROM detail_transaksi WHERE transaksi_id = $trx AND product_id = $productId ")->row();

        if ($detail == NULL) {
            $stok = $this->db->get_where('stock', ['product_id' => $productId])->row();

            $this->db->insert('detail_transaksi', ['transaksi_id' => $trx, 'product_id' => $productId, 'qty' => $qty]);
            $this->db->update('stock', ['jumlah' => $stok->jumlah - $qty], ['id' => $stok->id]);

            $message  = 'Insert';
        } else {
            $stok = $this->db->get_where('stock', ['product_id' => $productId])->row();

            $this->db->query("UPDATE detail_transaksi SET qty = $detail->qty + $qty WHERE id = $detail->id ");
            $this->db->update('stock', ['jumlah' => $stok->jumlah - $qty], ['id' => $stok->id]);

            $message  = 'Update';
        }

        echo json_encode([
            'message' => $message
        ]);
    }

    public function update()
    {
        $trx = $this->input->post('trx', true);
        $product = $this->input->post('product', true);
        $qty = $this->input->post('qty', true);

        $stok = $this->db->get_where('stock', ['product_id' => $product])->row();
        $detail = $this->db->query("SELECT * FROM detail_transaksi WHERE transaksi_id = $trx AND product_id = $product ")->row();

        $this->db->update('stock', ['jumlah' => ($stok->jumlah + $detail->qty) - $qty], ['id' => $stok->id]);
        $this->db->query("UPDATE detail_transaksi SET qty = 0 WHERE id = $detail->id ");
        $this->db->query("UPDATE detail_transaksi SET qty = $qty WHERE id = $detail->id ");
        $message  = 'Update';


        echo json_encode([
            'message' => $message
        ]);
    }


    public function delete()
    {
        $trx = $this->input->get('trx', true);
        $product = $this->input->get('product', true);

        $stok = $this->db->get_where('stock', ['product_id' => $product])->row();
        $detail = $this->db->query("SELECT * FROM detail_transaksi WHERE transaksi_id = $trx AND product_id = $product ")->row();

        $this->db->update('stock', ['jumlah' => ($stok->jumlah + $detail->qty)], ['id' => $stok->id]);
        $this->db->query("DELETE FROM detail_transaksi WHERE transaksi_id = $trx AND product_id = $product ");

        echo json_encode([
            'message' => "Success"
        ]);
    }
}
