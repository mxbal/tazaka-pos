<?php

class StockController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();

        $this->load->model('Stock');
        $this->load->model('Supplier');
        $this->load->model('Product');
    }

    public function index()
    {
        $data['title'] = 'Data Stock';
        $data['breadcrumb'] = 'Data Stock';
        $data['url_breadcrumb'] = base_url('stock');

        $data['stock'] = $this->Stock->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('stock/index');
        $this->load->view('layouts/footer');
    }

    // public function create()
    // {
    //     $data['title'] = 'Tambah Stock';
    //     $data['breadcrumb'] = 'Tambah Stock';
    //     $data['url_breadcrumb'] = base_url('stock');

    //     $data['action'] = base_url('stock/store');
    //     $data['product'] = $this->Product->get();
    //     $data['supplier'] = $this->Supplier->get();

    //     $this->load->view('layouts/header', $data);
    //     $this->load->view('stock/form');
    //     $this->load->view('layouts/footer');
    // }

    // public function store()
    // {
    //     $validation = [
    //         [
    //             'field' => 'supplier',
    //             'label' => 'Supplier',
    //             'rules' => 'required'
    //         ],
    //     ];

    //     $this->form_validation->set_rules($validation);

    //     if ($this->form_validation->run() == false) {
    //         $this->create();
    //     } else {
    //         $products = $this->input->post('product', true);
    //         $jumlah = $this->input->post('jumlah', true);

    //         foreach ($products as $prod => $value) {
    //             $stok = $this->db->get_where('stock', ['product_id' => $value])->row();

    //             if ($stok->product_id == $value) {
    //                 $data[$prod] = [
    //                     'supplier_id' => $this->input->post('supplier', true),
    //                     'product_id' => $value,
    //                     'jumlah' => $jumlah[$prod] + $stok->jumlah
    //                 ];

    //                 $this->db->update('stock', $data[$prod], ['id' => $stok->id]);
    //             } else {
    //                 $data[$prod] = [
    //                     'supplier_id' => $this->input->post('supplier', true),
    //                     'product_id' => $value,
    //                     'jumlah' => $jumlah[$prod]
    //                 ];

    //                 $this->db->insert('stock', $data[$prod]);
    //             }
    //         }

    //         $this->session->set_flashdata('success', 'Stock berhasil ditambahkan');
    //         redirect(base_url('stock'));
    //     }
    // }

    // public function edit($id)
    // {
    //     $data['title'] = 'Edit stock';
    //     $data['breadcrumb'] = 'Edit stock';
    //     $data['url_breadcrumb'] = base_url('stock');

    //     $data['action'] = base_url('stock/update/' . $id);
    //     $data['supplier'] = $this->Supplier->get();
    //     $data['product'] = $this->Product->get();
    //     $data['stock'] = $this->Stock->find($id);

    //     $this->load->view('layouts/header', $data);
    //     $this->load->view('stock/form');
    //     $this->load->view('layouts/footer');
    // }

    // public function update($id)
    // {
    //     $validation = [
    //         [
    //             'field' => 'supplier',
    //             'label' => 'Supplier',
    //             'rules' => 'required'
    //         ],
    //         [
    //             'field' => 'stock-product',
    //             'label' => 'Product',
    //             'rules' => 'required'
    //         ],
    //         [
    //             'field' => 'jumlah',
    //             'label' => 'Jumlah',
    //             'rules' => 'required'
    //         ],
    //     ];

    //     $this->form_validation->set_rules($validation);

    //     if ($this->form_validation->run() == false) {
    //         $this->edit($id);
    //     } else {
    //         $data = [
    //             'supplier_id' => $this->input->post('supplier', true),
    //             'product_id' => $this->input->post('stock-product', true),
    //             'jumlah' => $this->input->post('jumlah', true),
    //         ];

    //         $this->Stock->update($id, $data);

    //         $this->session->set_flashdata('success', 'Stock berhasil diupdate');
    //         redirect(base_url('stock'));
    //     }
    // }

    // public function delete($id)
    // {
    //     $this->Stock->delete($id);

    //     $this->session->set_flashdata('success', 'Stock berhasil dihapus');
    //     redirect(base_url('stock'));
    // }
}
