<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

date_default_timezone_set('Asia/Jakarta');

class ProductController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();

        $this->load->model('Product');
        $this->load->model('Kategori');
        $this->load->model('Satuan');
    }

    public function index()
    {
        $data['title'] = 'Data Product';
        $data['breadcrumb'] = 'Data Product';
        $data['url_breadcrumb'] = base_url('product');

        $data['product'] = $this->Product->get();
        $this->load->view('layouts/header', $data);
        $this->load->view('product/index');
        $this->load->view('layouts/footer');
    }

    public function create()
    {
        $data['title'] = 'Tambah Product';
        $data['breadcrumb'] = 'Tambah Product';
        $data['url_breadcrumb'] = base_url('product');

        $data['action'] = base_url('product/store');
        $data['kategori'] = $this->Kategori->get();
        $data['satuan'] = $this->Satuan->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('product/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'kode_product',
                'label' => 'Kode Product',
                'rules' => 'required|is_unique[product.kode_product]'
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
            [
                'field' => 'kategori',
                'label' => 'Kategori',
                'rules' => 'required'
            ],
            [
                'field' => 'satuan',
                'label' => 'Satuan',
                'rules' => 'required'
            ],
            [
                'field' => 'harga_jual',
                'label' => 'Harga Jual',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data = [
                'satuan_id' => $this->input->post('satuan', true),
                'kategori_id' => $this->input->post('kategori', true),
                'kode_product' => $this->input->post('kode_product', true),
                'nama' => $this->input->post('nama', true),
                'slug' => url_title($this->input->post('kode_product', true), "-", true),
                // 'harga_beli' => $this->input->post('harga_beli', true),
                'harga_jual' => $this->input->post('harga_jual', true),
            ];

            $product = $this->Product->create($data);
            $this->db->insert('stock', ['product_id' => $product, 'jumlah' => 0]);

            $this->session->set_flashdata('success', 'Product berhasil ditambahkan');
            redirect(base_url('product'));
        }
    }

    public function show($slug)
    {
        $product = $this->Product->find($slug);

        echo json_encode($product);
    }

    public function edit($slug)
    {
        $data['title'] = 'Edit Product';
        $data['breadcrumb'] = 'Edit Product';
        $data['url_breadcrumb'] = base_url('product');

        $data['action'] = base_url('product/update/' . $slug);
        $data['kategori'] = $this->Kategori->get();
        $data['satuan'] = $this->Satuan->get();
        $data['product'] = $this->Product->find($slug);

        $this->load->view('layouts/header', $data);
        $this->load->view('product/form');
        $this->load->view('layouts/footer');
    }

    public function update($slug)
    {
        $validation = [

            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
            // [
            //     'field' => 'harga_beli',
            //     'label' => 'Harga Beli',
            //     'rules' => 'required'
            // ],
            [
                'field' => 'kategori',
                'label' => 'Kategori',
                'rules' => 'required'
            ],
            [
                'field' => 'satuan',
                'label' => 'Satuan',
                'rules' => 'required'
            ],
            [
                'field' => 'harga_jual',
                'label' => 'Harga Jual',
                'rules' => 'required'
            ],
        ];

        $product = $this->Product->find($slug);

        if ($this->input->post('kode_product', true) == $product->kode_product) {
            array_push($validation, [
                'field' => 'kode_product',
                'label' => 'Kode Product',
                'rules' => 'required'
            ],);
        } else {
            array_push($validation, [
                'field' => 'kode_product',
                'label' => 'Kode Product',
                'rules' => 'required|is_unique[product.kode_product]'
            ],);
        }

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($slug);
        } else {
            $data = [
                'kategori_id' => $this->input->post('kategori', true),
                'satuan_id' => $this->input->post('satuan', true),
                'kode_product' => $this->input->post('kode_product', true),
                'nama' => $this->input->post('nama', true),
                'slug' => url_title($this->input->post('kode_product', true), "-", true),
                'harga_jual' => $this->input->post('harga_jual', true),
            ];

            $this->Product->update($slug, $data);

            $this->session->set_flashdata('success', 'Product berhasil diupdate');
            redirect(base_url('product'));
        }
    }

    public function delete($slug)
    {
        $this->Product->delete($slug);

        $this->session->set_flashdata('success', 'product berhasil dihapus');
        redirect(base_url('product'));
    }

    public function getkode()
    {
        $kode = date('dmY') . rand(1000, 9999);

        echo json_encode([
            'kode' => $kode
        ]);
    }

    public function get()
    {
        $search = $this->input->post('search', true);
        $query = $this->db->query("SELECT * FROM product WHERE kode_product LIKE '%$search%' OR nama LIKE '%$search%' ")->result();
        $result = [];

        foreach ($query as $q) {
            $result[] = [
                'label' => $q->kode_product . ' - ' . $q->nama,
                'value' => $q->id
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function laporan()
    {
        $data['title'] = 'Data Penjualan';
        $data['breadcrumb'] = 'Data Penjualan';
        $data['url_breadcrumb'] = base_url('laporan/penjualan');

        if ($this->input->get('from', true) && $this->input->get('to', true)) {
            $from = date('Y-m-d H:i:s', strtotime($this->input->get('from', true)));
            $to = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($this->input->get('to', true))));

            $data['penjualan'] = $this->Product->penjualan($from, $to);
        } else {
            $data['penjualan'] = $this->Product->penjualan();
        }


        $this->load->view('layouts/header', $data);
        $this->load->view('product/laporan');
        $this->load->view('layouts/footer');
    }

    public function export()
    {
        if ($this->input->get('from', true) && $this->input->get('to', true)) {
            $from = date('Y-m-d H:i:s', strtotime($this->input->get('from', true)));
            $to = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($this->input->get('to', true))));

            $penjualan = $this->Product->penjualan($from, $to);
        } else {
            $penjualan = $this->Product->penjualan();
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Kode Product');
        $sheet->setCellValue('C1', 'Nama Product');
        $sheet->setCellValue('D1', 'Stock');
        $sheet->setCellValue('E1', 'Terjual');
        $sheet->setCellValue('F1', 'Tersedia');
        $sheet->setCellValue('G1', 'Harga Beli');
        $sheet->setCellValue('H1', 'Harga Jual');
        $sheet->setCellValue('I1', 'Total Penjualan');
        $sheet->setCellValue('J1', 'Keuntungan');
        $rows = 2;
        $no = 1;
        $beli = 0;
        $jual = 0;
        $total = 0;
        $untung = 0;

        foreach ($penjualan as $val) {
            $beli += $val->harga_beli;
            $jual += $val->harga_jual;
            $total += $val->harga_jual * $this->db->query("SELECT SUM(detail_transaksi.qty) as terjual FROM detail_transaksi WHERE product_id = '$val->id' ")->row()->terjual;
            $untung += ($val->harga_jual - $val->harga_beli) * $this->db->query("SELECT SUM(detail_transaksi.qty) as terjual FROM detail_transaksi WHERE product_id = '$val->id' ")->row()->terjual;

            $sheet->setCellValue('A' . $rows, $no++);
            $sheet->setCellValue('B' . $rows, strval($val->kode_product));
            $sheet->setCellValue('C' . $rows, $val->nama);
            $sheet->setCellValue('D' . $rows, $this->db->query("SELECT SUM(detail_pembelian.jumlah) as jml FROM detail_pembelian WHERE product_id = '$val->id' ")->row()->jml . ' ' . $val->satuan);
            $sheet->setCellValue('E' . $rows, $this->db->query("SELECT SUM(detail_transaksi.qty) as terjual FROM detail_transaksi WHERE product_id = '$val->id' ")->row()->terjual . ' ' . $val->satuan);
            $sheet->setCellValue('F' . $rows, $val->stok . ' ' . $val->satuan);
            $sheet->setCellValue('G' . $rows, number_format($val->harga_beli, 2, ',', '.'));
            $sheet->setCellValue('H' . $rows, number_format($val->harga_jual, 2, '.', '.'));
            $sheet->setCellValue('I' . $rows, number_format($val->harga_jual * $this->db->query("SELECT SUM(detail_transaksi.qty) as terjual FROM detail_transaksi WHERE product_id = '$val->id' ")->row()->terjual, 2, ',', '.'));
            $sheet->setCellValue('J' . $rows, number_format(($val->harga_jual - $val->harga_beli) * $this->db->query("SELECT SUM(detail_transaksi.qty) as terjual FROM detail_transaksi WHERE product_id = '$val->id' ")->row()->terjual, 2, ',', '.'));
            $rows++;
        }

        $filename = 'Laporan Penjualan';

        $writer = new Xlsx($spreadsheet);
        header("Content-Type: application/vnd.ms-excel");
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }
}
