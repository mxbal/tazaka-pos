<?php

class PembelianController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();

        $this->load->model('Pembelian');
        $this->load->model('Product');
        $this->load->model('Supplier');
    }

    public function index()
    {
        $data['title'] = 'Data Pembelian';
        $data['breadcrumb'] = 'Data Pembelian';
        $data['url_breadcrumb'] = base_url('pembelian');

        $data['pembelian'] = $this->Pembelian->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('pembelian/index');
        $this->load->view('layouts/footer');
    }

    public function create()
    {
        $data['title'] = 'Tambah Pembelian';
        $data['breadcrumb'] = 'Tambah Pembelian';
        $data['url_breadcrumb'] = base_url('pembelian');

        $data['action'] = base_url('pembelian/store');
        $data['product'] = $this->Product->get();
        $data['supplier'] = $this->Supplier->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('pembelian/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'supplier',
                'label' => 'Supplier',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $products = $this->input->post('product', true);
            $jumlah = $this->input->post('jumlah', true);
            $harga = $this->input->post('harga', true);

            $dataPembelian = [
                'no_pembelian' => "PMB" . date('Ymd') . rand(100, 999),
                'supplier_id' => $this->input->post('supplier', true),
                'tanggal_pembelian' => $this->input->post('tgl', true)
            ];

            $total = 0;
            $pembelian = $this->Pembelian->create($dataPembelian);

            foreach ($products as $prod => $value) {
                $stok = $this->db->get_where('stock', ['product_id' => $value])->row();

                if ($stok->product_id == $value) {
                    $data[$prod] = [
                        'product_id' => $value,
                        'jumlah' => $jumlah[$prod] + $stok->jumlah
                    ];

                    $this->db->update('stock', $data[$prod], ['id' => $stok->id]);
                } else {
                    $data[$prod] = [
                        'product_id' => $value,
                        'jumlah' => $jumlah[$prod]
                    ];

                    $this->db->insert('stock', $data[$prod]);
                }
                $product = $this->db->get_where('product', ['id' => $value])->row();

                $this->db->update('product', [
                    'harga_beli' => floatval(((intval($product->harga_beli * $stok->jumlah) + intval($harga[$prod] * $jumlah[$prod])) / intval($stok->jumlah + $jumlah[$prod])))
                ], ['id' => $value]);


                $this->db->insert(
                    'detail_pembelian',
                    [
                        'pembelian_id' => $pembelian,
                        'product_id' => $value,
                        'jumlah' => $jumlah[$prod],
                        'harga' => $harga[$prod],
                        'total' => $jumlah[$prod] * $harga[$prod]
                    ]
                );

                $total += $jumlah[$prod] * $harga[$prod];
            }

            $this->db->update('pembelian', ['total' => $total], ['id' => $pembelian]);

            $this->session->set_flashdata('success', 'Pembelian berhasil ditambahkan');
            redirect(base_url('pembelian'));
        }
    }

    public function edit($nopmb)
    {
        $data['title'] = 'Edit Pembelian';
        $data['breadcrumb'] = 'Edit Pembelian';
        $data['url_breadcrumb'] = base_url('pembelian');

        $data['action'] = base_url('pembelian/update/' . $nopmb);
        $data['pembelian'] = $this->Pembelian->find($nopmb);
        $data['product'] = $this->Product->get();
        $data['supplier'] = $this->Supplier->get();
        $data['detail'] = $this->db->get_where('detail_pembelian', ['pembelian_id' => $data['pembelian']->id])->result();

        $this->load->view('layouts/header', $data);
        $this->load->view('pembelian/form');
        $this->load->view('layouts/footer');
    }

    public function update($nopmb)
    {
        $validation = [
            [
                'field' => 'supplier',
                'label' => 'Supplier',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($nopmb);
        } else {
            $products = $this->input->post('product', true);
            $jumlah = $this->input->post('jumlah', true);
            $harga = $this->input->post('harga', true);
            $pembelian = $this->Pembelian->find($nopmb);
            $total = 0;

            $dataPembelian = [
                'supplier_id' => $this->input->post('supplier', true),
                'tanggal_pembelian' => $this->input->post('tgl', true),
                'total' => 0,
            ];

            $this->Pembelian->update($nopmb, $dataPembelian);
            $details = $this->db->get_where('detail_pembelian', ['pembelian_id' => $pembelian->id])->result_array();

            foreach ($products as $prod => $value) {
                $stok = $this->db->get_where('stock', ['product_id' => $value])->row();
                $detail = $this->db->get_where('detail_pembelian', ['product_id' => $value])->row();

                if ($stok->product_id == $value) {
                    $data[$prod] = [
                        'product_id' => $value,
                        'jumlah' => $stok->jumlah - $detail->jumlah + $jumlah[$prod]
                    ];

                    $this->db->update('stock', $data[$prod], ['id' => $stok->id]);
                } else {
                    $data[$prod] = [
                        'product_id' => $value,
                        'jumlah' => $jumlah[$prod]
                    ];

                    $this->db->insert('stock', $data[$prod]);
                }

                $this->db->update(
                    'detail_pembelian',
                    [
                        'product_id' => $value,
                        'jumlah' => $jumlah[$prod],
                        'harga' => $harga[$prod],
                        'total' => $jumlah[$prod] * $harga[$prod]
                    ],
                    [
                        'id' => $details[$prod]['id']
                    ]
                );

                $total += $jumlah[$prod] * $harga[$prod];
            }

            $this->Pembelian->update($nopmb, ['total' => $total]);

            $this->session->set_flashdata('success', 'Pembelian berhasil diupdate');
            redirect(base_url('pembelian'));
        }
    }

    public function delete($pmb)
    {
        $pembelian = $this->Pembelian->find($pmb);
        $details = $this->db->get_where('detail_pembelian', ['pembelian_id' => $pembelian->id])->result();

        foreach ($details as $detail) {
            $stok = $this->db->get_where('stock', ['product_id' => $detail->product_id])->row();
            $this->db->update('stock', ['jumlah' => $stok->jumlah - $detail->jumlah], ['id' => $stok->id]);

            $this->db->delete('detail_pembelian', ['id' => $detail->id]);
        }

        $this->Pembelian->delete($pmb);

        $this->session->set_flashdata('success', 'Pembelian berhasil dihapus');
        redirect(base_url('pembelian'));
    }
}
