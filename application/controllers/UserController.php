<?php

class UserController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();

        $this->load->model('User');
    }

    public function index()
    {
        $data['title'] = 'Data User';
        $data['breadcrumb'] = 'Data User';
        $data['url_breadcrumb'] = base_url('user');

        $data['users'] = $this->User->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('user/index');
        $this->load->view('layouts/footer');
    }

    public function create()
    {
        $data['title'] = 'Tambah User';
        $data['breadcrumb'] = 'Tambah User';
        $data['url_breadcrumb'] = base_url('user');

        $data['action'] = base_url('user/store');

        $this->load->view('layouts/header', $data);
        $this->load->view('user/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|is_unique[users.username]'
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ],
            [
                'field' => 'level',
                'label' => 'Level',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            if (!empty($_FILES['image']['name'])) {

                $config['upload_path'] = 'uploads/users/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtolower($this->input->post('username', true)) . '_' . $_FILES['image']['name'];

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $image = $this->upload->data();
                    $filename = $image['file_name'];

                    $data = [
                        'username' => $this->input->post('username', true),
                        'nama' => $this->input->post('nama', true),
                        'password' => password_hash($this->input->post('password', true), PASSWORD_DEFAULT),
                        'level' => $this->input->post('level', true),
                        'image' => $filename,
                    ];

                    $this->User->create($data);

                    $this->session->set_flashdata('success', 'User berhasil ditambahkan');
                    redirect(base_url('user'));
                } else {
                    redirect(base_url('user/create'));
                }
            }
        }
    }

    public function edit($id)
    {
        $data['title'] = 'Edit User';
        $data['breadcrumb'] = 'Edit User';
        $data['url_breadcrumb'] = base_url('user');

        $data['action'] = base_url('user/update/' . $id);
        $data['user'] = $this->User->find($id);

        $this->load->view('layouts/header', $data);
        $this->load->view('user/form');
        $this->load->view('layouts/footer');
    }

    public function update($id)
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
            [
                'field' => 'level',
                'label' => 'Level',
                'rules' => 'required'
            ],
        ];

        $user = $this->User->find($id);

        if ($this->input->post('username', true) == $user->username) {
            array_push($validation, [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ],);
        } else {
            array_push($validation, [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|is_unique[users.username]'
            ],);
        }

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($id);
        } else {
            if ($_FILES['image']['name'] != '') {

                $config['upload_path'] = 'uploads/users/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtolower($this->input->post('username', true)) . '_' . $_FILES['image']['name'];

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $image = $this->upload->data();
                    unlink('uploads/users/' . $user->image);
                    $filename = $image['file_name'];
                } else {
                    $filename = $user->image;
                }
            } else {
                $filename = $user->image;
            }

            $data = [
                'username' => $this->input->post('username', true),
                'nama' => $this->input->post('nama', true),
                'password' => !empty($this->input->post('password', true)) ? password_hash($this->input->post('password', true), PASSWORD_DEFAULT) : $user->password,
                'level' => $this->input->post('level', true),
                'image' => $filename,
            ];

            $this->User->update($id, $data);

            $this->session->set_flashdata('success', 'User berhasil diupdate');
            redirect(base_url('user'));
        }
    }

    public function delete($id)
    {
        $user = $this->User->find($id);
        unlink('uploads/users/' . $user->image);

        $this->User->delete($id);

        $this->session->set_flashdata('success', 'User berhasil dihapus');
        redirect(base_url('user'));
    }
}
