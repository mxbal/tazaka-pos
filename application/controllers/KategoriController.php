<?php

class KategoriController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();

        $this->load->model('Kategori');
    }

    public function index()
    {
        $data['title'] = 'Data Kategori';
        $data['breadcrumb'] = 'Data Kategori';
        $data['url_breadcrumb'] = base_url('kategori');

        $data['kategori'] = $this->Kategori->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('kategori/index');
        $this->load->view('layouts/footer');
    }

    public function create()
    {
        $data['title'] = 'Tambah Kategori';
        $data['breadcrumb'] = 'Tambah Kategori';
        $data['url_breadcrumb'] = base_url('kategori');

        $data['action'] = base_url('kategori/store');

        $this->load->view('layouts/header', $data);
        $this->load->view('kategori/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data = [
                'nama' => $this->input->post('nama', true),
                'slug' => url_title($this->input->post('nama', true), "-", true),
            ];

            $this->Kategori->create($data);

            $this->session->set_flashdata('success', 'Kategori berhasil ditambahkan');
            redirect(base_url('kategori'));
        }
    }

    public function show($slug)
    {
        $data['title'] = 'Detail Kategori';
        $data['breadcrumb'] = 'Detail Kategori';
        $data['url_breadcrumb'] = base_url('kategori');

        $data['kategori'] = $this->Kategori->find($slug);
        $data['subs'] = $this->db->get_where('sub_kategori', ['kategori_id' => $data['kategori']->id])->result();

        $this->load->view('layouts/header', $data);
        $this->load->view('kategori/show');
        $this->load->view('layouts/footer');
    }

    public function edit($slug)
    {
        $data['title'] = 'Edit Kategori';
        $data['breadcrumb'] = 'Edit Kategori';
        $data['url_breadcrumb'] = base_url('kategori');

        $data['action'] = base_url('kategori/update/' . $slug);
        $data['kategori'] = $this->Kategori->find($slug);

        $this->load->view('layouts/header', $data);
        $this->load->view('kategori/form');
        $this->load->view('layouts/footer');
    }

    public function update($slug)
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($slug);
        } else {
            $data = [
                'nama' => $this->input->post('nama', true),
                'slug' => url_title($this->input->post('nama', true), "-", true),
            ];

            $this->Kategori->update($slug, $data);

            $this->session->set_flashdata('success', 'Kategori berhasil diupdate');
            redirect(base_url('kategori'));
        }
    }

    public function delete($slug)
    {
        $this->Kategori->delete($slug);

        $this->session->set_flashdata('success', 'Kategori berhasil dihapus');
        redirect(base_url('kategori'));
    }
}
