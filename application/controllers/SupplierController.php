<?php

class SupplierController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();

        $this->load->model('Supplier');
    }

    public function index()
    {
        $data['title'] = 'Data Supplier';
        $data['breadcrumb'] = 'Data Supplier';
        $data['url_breadcrumb'] = base_url('supplier');

        $data['supplier'] = $this->Supplier->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('supplier/index');
        $this->load->view('layouts/footer');
    }

    public function create()
    {
        $data['title'] = 'Tambah Supplier';
        $data['breadcrumb'] = 'Tambah Supplier';
        $data['url_breadcrumb'] = base_url('supplier');

        $data['action'] = base_url('supplier/store');

        $this->load->view('layouts/header', $data);
        $this->load->view('supplier/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required'
            ],
            [
                'field' => 'telp',
                'label' => 'No Telepon',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data = [
                'nama' => $this->input->post('nama', true),
                'slug' => url_title($this->input->post('nama', true), "-", true),
                'alamat' => $this->input->post('alamat', true),
                'telp' => $this->input->post('telp', true),
            ];

            $this->Supplier->create($data);

            $this->session->set_flashdata('success', 'Supplier berhasil ditambahkan');
            redirect(base_url('supplier'));
        }
    }

    public function edit($slug)
    {
        $data['title'] = 'Edit Supplier';
        $data['breadcrumb'] = 'Edit Supplier';
        $data['url_breadcrumb'] = base_url('supplier');

        $data['action'] = base_url('supplier/update/' . $slug);
        $data['supplier'] = $this->Supplier->find($slug);

        $this->load->view('layouts/header', $data);
        $this->load->view('supplier/form');
        $this->load->view('layouts/footer');
    }

    public function update($slug)
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required'
            ],
            [
                'field' => 'telp',
                'label' => 'No Telepon',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($slug);
        } else {
            $data = [
                'nama' => $this->input->post('nama', true),
                'slug' => url_title($this->input->post('nama', true), "-", true),
                'alamat' => $this->input->post('alamat', true),
                'telp' => $this->input->post('telp', true),
            ];

            $this->Supplier->update($slug, $data);

            $this->session->set_flashdata('success', 'Supplier berhasil diupdate');
            redirect(base_url('supplier'));
        }
    }

    public function delete($slug)
    {
        $this->Supplier->delete($slug);

        $this->session->set_flashdata('success', 'Supplier berhasil dihapus');
        redirect(base_url('supplier'));
    }
}
