<?php

class LostProductController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();

        $this->load->model('LostProduct');
        $this->load->model('Product');
    }

    public function index()
    {
        $data['title'] = 'Data Lost Product';
        $data['breadcrumb'] = 'Data Lost Product';
        $data['url_breadcrumb'] = base_url('lost-product');

        $data['product'] = $this->LostProduct->get();
        $this->load->view('layouts/header', $data);
        $this->load->view('lost-product/index');
        $this->load->view('layouts/footer');
    }

    public function create()
    {
        $data['title'] = 'Tambah Lost Product';
        $data['breadcrumb'] = 'Tambah Lost Product';
        $data['url_breadcrumb'] = base_url('lost-product');

        $data['action'] = base_url('lost-product/store');
        $data['product'] = $this->Product->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('lost-product/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'product',
                'label' => 'Product',
                'rules' => 'required'
            ],
            [
                'field' => 'jumlah',
                'label' => 'Jumlah',
                'rules' => 'required'
            ],
            [
                'field' => 'ket',
                'label' => 'Keterangan',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data = [
                'product_id' => $this->input->post('product', true),
                'jumlah' => $this->input->post('jumlah', true),
                'ket' => $this->input->post('ket', true),
            ];

            $stok = $this->db->get_where('stock', ['product_id' => $this->input->post('product', true)])->row();

            $sisa = intval($stok->jumlah - $this->input->post('jumlah', true));
            $this->db->update('stock', ['jumlah' => $sisa], ['product_id' => $stok->product_id]);

            $this->LostProduct->create($data);

            $this->session->set_flashdata('success', 'Lost Product berhasil ditambahkan');
            redirect(base_url('lost-product'));
        }
    }

    public function show($id)
    {
        $product = $this->LostProduct->find($id);

        echo json_encode($product);
    }

    public function edit($id)
    {
        $data['title'] = 'Edit Lost Product';
        $data['breadcrumb'] = 'Edit Lost Product';
        $data['url_breadcrumb'] = base_url('lost-product');

        $data['action'] = base_url('lost-product/update/' . $id);
        $data['product'] = $this->Product->get();
        $data['lost'] = $this->LostProduct->find($id);

        $this->load->view('layouts/header', $data);
        $this->load->view('lost-product/form');
        $this->load->view('layouts/footer');
    }

    public function update($id)
    {
        $validation = [
            [
                'field' => 'product',
                'label' => 'Product',
                'rules' => 'required'
            ],
            [
                'field' => 'jumlah',
                'label' => 'Jumlah',
                'rules' => 'required'
            ],
            [
                'field' => 'ket',
                'label' => 'Keterangan',
                'rules' => 'required'
            ],
        ];


        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($id);
        } else {
            $product = $this->input->post('product', true);
            $jumlah = $this->input->post('jumlah', true);

            $lost = $this->db->get_where('lost', ['product_id' => $product])->row();
            $oldStock = $this->db->get_where('stock', ['product_id' => $product])->row();

            if ($jumlah > $lost->jumlah) {
                $hasil = $jumlah - $lost->jumlah;
                $jml = $oldStock->jumlah - $hasil;

                $this->db->update('stock', ['jumlah' => $jml], ['product_id' => $product]);
            }

            if ($jumlah < $lost->jumlah) {
                $hasil = $lost->jumlah - $jumlah;
                $jml = $oldStock->jumlah + $hasil;

                $this->db->update('stock', ['jumlah' => $jml], ['product_id' => $product]);
            }

            $data = [
                'product_id' => $product,
                'jumlah' => $jumlah,
                'ket' => $this->input->post('ket', true),
            ];

            $this->LostProduct->update($id, $data);

            $this->session->set_flashdata('success', 'Lost Product berhasil diupdate');
            redirect(base_url('lost-product'));
        }
    }

    public function delete($id)
    {
        $lost = $this->db->get_where('lost', ['id' => $id])->row();
        $oldStock = $this->db->get_where('stock', ['product_id' => $lost->product_id])->row();

        $this->db->update('stock', ['jumlah' => $oldStock->jumlah + $lost->jumlah], ['id' => $oldStock->id]);
        $this->LostProduct->delete($id);

        $this->session->set_flashdata('success', 'Lost product berhasil dihapus');
        redirect(base_url('product'));
    }
}
