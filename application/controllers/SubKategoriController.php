<?php

class SubKategoriController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();
        isAdmin();

        $this->load->model('Kategori');
        $this->load->model('SubKategori');
    }

    public function index()
    {
        $data['title'] = 'Data Kategori';
        $data['breadcrumb'] = 'Data Kategori';
        $data['url_breadcrumb'] = base_url('kategori');

        $data['kategori'] = $this->Kategori->get();

        $this->load->view('layouts/header', $data);
        $this->load->view('kategori/index');
        $this->load->view('layouts/footer');
    }

    public function create($slug)
    {
        $data['title'] = 'Tambah Sub Kategori';
        $data['breadcrumb'] = 'Tambah Sub Kategori';
        $data['url_breadcrumb'] = base_url('subkategori');

        $data['action'] = base_url('subkategori/store');
        $data['kategori'] = $this->Kategori->find($slug);

        $this->load->view('layouts/header', $data);
        $this->load->view('subkategori/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'nama_sub',
                'label' => 'Nama Sub',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        $kategori = $this->db->get_where('kategori', ['id' => $this->input->post('kategori', true)])->row();

        if ($this->form_validation->run() == false) {
            $this->create($kategori->slug);
        } else {

            $data = [
                'kategori_id' => $this->input->post('kategori', true),
                'nama_sub' => $this->input->post('nama_sub', true),
                'slug' => url_title($this->input->post('nama_sub', true), "-", true),
            ];

            $this->SubKategori->create($data);

            $this->session->set_flashdata('success', 'Sub Kategori berhasil ditambahkan');
            redirect(base_url('kategori/' . $kategori->slug));
        }
    }

    public function edit($slug)
    {
        $data['title'] = 'Edit Kategori';
        $data['breadcrumb'] = 'Edit Kategori';
        $data['url_breadcrumb'] = base_url('kategori');

        $data['action'] = base_url('kategori/update/' . $slug);
        $data['kategori'] = $this->Kategori->find($slug);

        $this->load->view('layouts/header', $data);
        $this->load->view('kategori/form');
        $this->load->view('layouts/footer');
    }

    public function update($slug)
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($slug);
        } else {
            $data = [
                'nama' => $this->input->post('nama', true),
                'slug' => url_title($this->input->post('nama', true), "-", true),
            ];

            $this->Kategori->update($slug, $data);

            $this->session->set_flashdata('success', 'Kategori berhasil diupdate');
            redirect(base_url('kategori'));
        }
    }

    public function delete($slug)
    {
        $this->Kategori->delete($slug);

        $this->session->set_flashdata('success', 'Kategori berhasil dihapus');
        redirect(base_url('kategori'));
    }
}
