<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

date_default_timezone_set('Asia/Jakarta');

class PiutangController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        isLogin();

        $this->load->model('Piutang');
    }

    public function index()
    {
        $data['title'] = 'Data Piutang';
        $data['breadcrumb'] = 'Data Piutang';
        $data['url_breadcrumb'] = base_url('piutang');

        if ($this->session->userdata('level') != 'Admin') {
            $user = $this->session->userdata('id');
        } else {
            $user = '';
        }

        $data['piutang'] = $this->Piutang->get('', '', $user);

        $this->load->view('layouts/header', $data);
        $this->load->view('piutang/index');
        $this->load->view('layouts/footer');
    }

    public function create()
    {
        $data['title'] = 'Tambah piutang';
        $data['breadcrumb'] = 'Tambah piutang';
        $data['url_breadcrumb'] = base_url('piutang');

        $data['action'] = base_url('piutang/store');

        $this->load->view('layouts/header', $data);
        $this->load->view('piutang/form');
        $this->load->view('layouts/footer');
    }

    public function store()
    {
        $validation = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data = [
                'nama' => $this->input->post('nama', true),
                'slug' => url_title($this->input->post('nama', true), "-", true),
            ];

            $this->piutang->create($data);

            $this->session->set_flashdata('success', 'piutang berhasil ditambahkan');
            redirect(base_url('piutang'));
        }
    }

    public function edit($id)
    {
        $data['title'] = 'Edit Piutang';
        $data['breadcrumb'] = 'Edit Piutang';
        $data['url_breadcrumb'] = base_url('piutang');

        $data['action'] = base_url('piutang/update/' . $id);
        $data['piutang'] = $this->Piutang->find($id);

        $this->load->view('layouts/header', $data);
        $this->load->view('piutang/form');
        $this->load->view('layouts/footer');
    }

    public function update($id)
    {
        $validation = [
            [
                'field' => 'tanggal_bayar',
                'label' => 'Tanggal Bayar',
                'rules' => 'required'
            ],
            [
                'field' => 'bayar',
                'label' => 'Bayar',
                'rules' => 'required'
            ],
        ];

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == false) {
            $this->edit($id);
        } else {
            $piutang = $this->Piutang->find($id);
            // $transaksi = $this->db->get_where('transaksi', ['id' => $piutang->transaksi_id])->row();

            $dataTrx = [
                'bayar' => str_replace('.', '', $this->input->post('bayar', true)),
                'kembali' => str_replace('.', '', $this->input->post('kembali', true)),
                'status' => $this->input->post('status', true)
            ];

            $data = [
                'tgl_bayar' => $this->input->post('tanggal_bayar', true),
                'id' => url_title($this->input->post('nama', true), "-", true),
            ];

            $this->db->update('transaksi', $dataTrx, ['id' => $piutang->transaksi_id]);

            $this->Piutang->update($id, $data);

            $this->session->set_flashdata('success', 'Piutang berhasil diupdate');
            redirect(base_url('piutang'));
        }
    }

    public function delete($slug)
    {
        $this->piutang->delete($slug);

        $this->session->set_flashdata('success', 'Piutang berhasil dihapus');
        redirect(base_url('piutang'));
    }

    public function laporan()
    {
        $data['title'] = 'Data Piutang';
        $data['breadcrumb'] = 'Data Piutang';
        $data['url_breadcrumb'] = base_url('piutang');

        if ($this->input->get('from', true) && $this->input->get('to', true)) {
            $from = date('Y-m-d H:i:s', strtotime($this->input->get('from', true)));
            $to = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($this->input->get('to', true))));

            $data['piutang'] = $this->Piutang->get($from, $to);
        } else {
            $data['piutang'] = $this->Piutang->get();
        }


        $this->load->view('layouts/header', $data);
        $this->load->view('piutang/laporan');
        $this->load->view('layouts/footer');
    }

    public function export()
    {
        if ($this->input->get('from', true) && $this->input->get('to', true)) {
            $from = date('Y-m-d H:i:s', strtotime($this->input->get('from', true)));
            $to = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($this->input->get('to', true))));

            $piutang = $this->Piutang->get($from, $to, $id = '');
        } else {
            $piutang = $this->Piutang->get();
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Tanggal');
        $sheet->setCellValue('C1', 'Invoice');
        $sheet->setCellValue('D1', 'Jenis Pembayaran');
        $sheet->setCellValue('E1', 'Jenis Discount');
        $sheet->setCellValue('F1', 'Total');
        $sheet->setCellValue('G1', 'Discount');
        $sheet->setCellValue('H1', 'Bayar');
        $sheet->setCellValue('I1', 'Kembali');
        $sheet->setCellValue('J1', 'Member');
        $sheet->setCellValue('K1', 'Kasir');
        $sheet->setCellValue('l1', 'Jatuh Tempo');
        $sheet->setCellValue('M1', 'Tanggal Bayar');
        $sheet->setCellValue('N1', 'Status');
        $rows = 2;
        $no = 1;
        foreach ($piutang as $val) {
            $sheet->setCellValue('A' . $rows, $no++);
            $sheet->setCellValue('B' . $rows, date('Y-m-d H:i:s', strtotime($val->tanggal)));
            $sheet->setCellValue('C' . $rows, $val->invoice);
            $sheet->setCellValue('D' . $rows, $val->jenis_pembayaran);
            $sheet->setCellValue('E' . $rows, $val->jenis_diskon);
            $sheet->setCellValue('F' . $rows, number_format($val->total, 2, '.', '.'));
            $sheet->setCellValue('G' . $rows, $val->jenis_diskon == 'Persen' ? $val->discount . '%' : $val->discount);
            $sheet->setCellValue('H' . $rows, number_format($val->bayar, 2, '.', '.'));
            $sheet->setCellValue('I' . $rows, number_format($val->kembali, 2, '.', '.'));
            $sheet->setCellValue('J' . $rows, $this->db->get_where('member', ['id' => $val->member_id])->row()->nama ?? '-');
            $sheet->setCellValue('K' . $rows, $this->db->get_where('users', ['id' => $val->user_id])->row()->nama);
            $sheet->setCellValue('L' . $rows, date('Y-m-d H:i:s', strtotime($val->jatuh_tempo)));
            $sheet->setCellValue('M' . $rows, $val->tgl_bayar != NULL ? date('Y-m-d H:i:s', strtotime($val->tgl_bayar)) : '-');
            $sheet->setCellValue('N' . $rows, $val->status);
            $rows++;
        }

        $filename = 'Laporan Piutang';

        $writer = new Xlsx($spreadsheet);
        header("Content-Type: application/vnd.ms-excel");
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }
}
