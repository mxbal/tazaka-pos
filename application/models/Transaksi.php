<?php

class Transaksi extends CI_Model
{
    public function get($from = '', $to = '', $id = '')
    {
        $now = date('Y-m-d');

        if ($from == '' && $to == '') {
            if ($id == '') {
                return $this->db->query("SELECT * FROM transaksi WHERE tanggal LIKE '%$now%' ORDER BY id DESC ")->result();
            } else {
                return $this->db->query("SELECT * FROM transaksi WHERE tanggal LIKE '%$now%' AND user_id = '$id' ORDER BY id DESC ")->result();
            }
        } else {
            if ($id == '') {
                return $this->db->query("SELECT * FROM transaksi WHERE tanggal >= '$from' AND tanggal <= '$to' ORDER BY id DESC ")->result();
            } else {
                return $this->db->query("SELECT * FROM transaksi WHERE tanggal >= '$from' AND tanggal <= '$to' AND user_id = '$id' ORDER BY id DESC ")->result();
            }
        }
    }

    public function create($data)
    {
        $this->db->insert('transaksi', $data);
        return $this->db->insert_id();
    }

    public function find($invoice)
    {
        return $this->db->get_where('transaksi', ['invoice' => $invoice])->row();
    }

    public function update($invoice, $data)
    {
        $this->db->where('invoice', $invoice);
        $this->db->update('transaksi', $data);
    }

    public function delete($invoice)
    {
        $this->db->delete('transaksi', ['invoice' => $invoice]);
    }
}
