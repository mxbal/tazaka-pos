<?php

class Pembelian extends CI_Model
{
    public function get()
    {
        return $this->db->query("SELECT pembelian.*, supplier.nama as supplier FROM pembelian JOIN supplier ON pembelian.supplier_id=supplier.id")->result();
    }

    public function create($data)
    {
        $this->db->insert('pembelian', $data);
        return $this->db->insert_id();
    }

    public function find($nopmb)
    {
        return $this->db->get_where('pembelian', ['no_pembelian' => $nopmb])->row();
    }

    public function update($nopmb, $data)
    {
        $this->db->where('no_pembelian', $nopmb);
        $this->db->update('pembelian', $data);
    }

    public function delete($pmb)
    {
        $this->db->delete('pembelian', ['no_pembelian' => $pmb]);
    }
}
