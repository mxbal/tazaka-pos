<?php

class Stock extends CI_Model
{
    public function get()
    {
        $query = $this->db->query("SELECT stock.id, stock.jumlah, product.id as product_id, product.nama as nama_product, satuan.nama as satuan, product.satuan_id FROM stock JOIN product ON stock.product_id = product.id JOIN satuan ON product.satuan_id = satuan.id ");
        return $query->result();
    }

    public function create($data)
    {
        $this->db->insert('stock', $data);
    }

    public function find($id)
    {
        return $this->db->get_where('stock', ['id' => $id])->row();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('stock', $data);
    }

    public function delete($id)
    {
        $this->db->delete('stock', ['id' => $id]);
    }
}
