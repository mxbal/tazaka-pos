<?php

class Product extends CI_Model
{
    public function get()
    {
        return $this->db->get('product')->result();
    }

    public function create($data)
    {
        $this->db->insert('product', $data);
        return $this->db->insert_id();
    }

    public function find($slug)
    {
        return $this->db->get_where('product', ['slug' => $slug])->row();
    }

    public function where($where, $val)
    {
        return $this->db->get_where('product', [$where => $val])->row();
    }

    public function update($slug, $data)
    {
        $this->db->where('slug', $slug);
        $this->db->update('product', $data);
    }

    public function delete($slug)
    {
        $this->db->delete('product', ['slug' => $slug]);
    }

    public function penjualan($from = '', $to = '')
    {
        if ($from == '' && $to == '') {
            return $this->db->query("SELECT product.*, stock.jumlah as stok, satuan.nama as satuan FROM product JOIN stock ON product.id = stock.product_id JOIN satuan ON product.satuan_id = satuan.id")->result();
        } else {
            return $this->db->query("SELECT product.*, stock.jumlah as stok, satuan.nama as satuan FROM product JOIN stock ON product.id = stock.product_id JOIN satuan ON product.satuan_id = satuan.id")->result();
        }
    }
}
