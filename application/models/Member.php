<?php

class Member extends CI_Model
{
    public function get()
    {
        return $this->db->get('member')->result();
    }

    public function create($data)
    {
        $this->db->insert('member', $data);
    }

    public function find($kode)
    {
        return $this->db->get_where('member', ['kode_member' => $kode])->row();
    }

    public function update($kode, $data)
    {
        $this->db->where('kode_member', $kode);
        $this->db->update('member', $data);
    }

    public function delete($kode)
    {
        $this->db->delete('member', ['kode_member' => $kode]);
    }
}
