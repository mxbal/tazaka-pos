<?php

class Piutang extends CI_Model
{
    public function get($from = '', $to = '', $id = '')
    {
        $now = date('Y-m-d');

        if ($id == '') {
            return $this->db->query("SELECT piutang.id as piutang, piutang.jatuh_tempo, piutang.tgl_bayar, transaksi.*, transaksi.id as transaksi FROM piutang JOIN transaksi ON piutang.transaksi_id = transaksi.id ")->result();

            if ($from == '' && $to == '') {
                return $this->db->query("SELECT piutang.*, transaksi.*, transaksi.id as transaksi FROM piutang JOIN transaksi ON piutang.transaksi_id = transaksi.id WHERE transaksi.tanggal LIKE '%$now%' ORDER BY piutang.id DESC ")->result();
            } else {
                return $this->db->query("SELECT piutang.*, transaksi.*, transaksi.id as transaksi FROM piutang JOIN transaksi ON piutang.transaksi_id = transaksi.id WHERE transaksi.tanggal >= '$from' AND tanggal <= '$to' ORDER BY piutang.id DESC ")->result();
            }
        } else {
            return $this->db->query("SELECT piutang.*, transaksi.*, transaksi.id as transaksi FROM piutang JOIN transaksi ON piutang.transaksi_id = transaksi.id WHERE transaksi.user_id = '$id'")->result();

            if ($from == '' && $to == '') {
                return $this->db->query("SELECT piutang.*, transaksi.*, transaksi.id as transaksi FROM piutang JOIN transaksi ON piutang.transaksi_id = transaksi.id WHERE transaksi.user_id = '$id' AND transaksi.tanggal LIKE '%$now%' ORDER BY piutang.id DESC ")->result();
            } else {
                return $this->db->query("SELECT piutang.*, transaksi.*, transaksi.id as transaksi FROM piutang JOIN transaksi ON piutang.transaksi_id = transaksi.id WHERE transaksi.user_id = '$id' AND transaksi.tanggal >= '$from' AND tanggal <= '$to' ORDER BY piutang.id DESC ")->result();
            }
        }
    }

    public function create($data)
    {
        $this->db->insert('piutang', $data);
    }

    public function find($id)
    {
        return $this->db->query("SELECT piutang.*, transaksi.total, transaksi.invoice, transaksi.user_id, transaksi.tanggal, transaksi.status, transaksi.bayar, transaksi.kembali, transaksi.id as transaksi FROM piutang JOIN transaksi ON piutang.transaksi_id = transaksi.id WHERE piutang.id = $id ")->row();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('piutang', $data);
    }

    public function delete($id)
    {
        $this->db->delete('piutang', ['id' => $id]);
    }
}
