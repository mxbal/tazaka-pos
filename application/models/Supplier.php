<?php

class Supplier extends CI_Model
{
    public function get()
    {
        return $this->db->get('supplier')->result();
    }

    public function create($data)
    {
        $this->db->insert('supplier', $data);
    }

    public function find($slug)
    {
        return $this->db->get_where('supplier', ['slug' => $slug])->row();
    }

    public function update($slug, $data)
    {
        $this->db->where('slug', $slug);
        $this->db->update('supplier', $data);
    }

    public function delete($slug)
    {
        $this->db->delete('supplier', ['slug' => $slug]);
    }
}
