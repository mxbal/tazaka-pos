<?php

class SubKategori extends CI_Model
{
    public function get()
    {
        return $this->db->get('sub_kategori')->result();
    }

    public function create($data)
    {
        $this->db->insert('sub_kategori', $data);
    }

    public function find($slug)
    {
        return $this->db->get_where('sub_kategori', ['slug' => $slug])->row();
    }

    public function update($slug, $data)
    {
        $this->db->where('slug', $slug);
        $this->db->update('sub_kategori', $data);
    }

    public function delete($slug)
    {
        $this->db->delete('sub_kategori', ['slug' => $slug]);
    }
}
