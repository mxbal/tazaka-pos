<?php

class Kategori extends CI_Model
{
    public function get()
    {
        return $this->db->get('kategori')->result();
    }

    public function create($data)
    {
        $this->db->insert('kategori', $data);
    }

    public function find($slug)
    {
        return $this->db->get_where('kategori', ['slug' => $slug])->row();
    }

    public function update($slug, $data)
    {
        $this->db->where('slug', $slug);
        $this->db->update('kategori', $data);
    }

    public function delete($slug)
    {
        $this->db->delete('kategori', ['slug' => $slug]);
    }
}
