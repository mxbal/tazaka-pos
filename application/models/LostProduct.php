<?php

class LostProduct extends CI_Model
{
    public function get()
    {
        return $this->db->query("SELECT lost.*, product.nama FROM lost JOIN product ON lost.product_id=product.id")->result();
    }

    public function create($data)
    {
        $this->db->insert('lost', $data);
    }

    public function find($id)
    {
        return $this->db->get_where('lost', ['id' => $id])->row();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('lost', $data);
    }

    public function delete($id)
    {
        $this->db->delete('lost', ['id' => $id]);
    }
}
