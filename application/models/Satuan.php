<?php

class Satuan extends CI_Model
{
    public function get()
    {
        return $this->db->get('satuan')->result();
    }

    public function create($data)
    {
        $this->db->insert('satuan', $data);
    }

    public function find($slug)
    {
        return $this->db->get_where('satuan', ['slug' => $slug])->row();
    }

    public function update($slug, $data)
    {
        $this->db->where('slug', $slug);
        $this->db->update('satuan', $data);
    }

    public function delete($slug)
    {
        $this->db->delete('satuan', ['slug' => $slug]);
    }
}
