<?php

class User extends CI_Model
{
    public function get()
    {
        return $this->db->get('users')->result();
    }

    public function create($data)
    {
        $this->db->insert('users', $data);
    }

    public function find($id)
    {
        return $this->db->get_where('users', ['id' => $id])->row();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function delete($id)
    {
        $this->db->delete('users', ['id' => $id]);
    }
}
