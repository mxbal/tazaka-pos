<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'AuthController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Route Auth
$route['login'] = 'AuthController/login';
$route['logout'] = 'AuthController/logout';
$route['dashboard'] = 'DashboardController';

// Route User
$route['user'] = 'UserController';
$route['user/create'] = 'UserController/create';
$route['user/store'] = 'UserController/store';
$route['user/edit/(:num)'] = 'UserController/edit/$1';
$route['user/update/(:num)'] = 'UserController/update/$1';
$route['user/delete/(:num)'] = 'UserController/delete/$1';

// Route Member
$route['member'] = 'MemberController';
$route['member/create'] = 'MemberController/create';
$route['member/store'] = 'MemberController/store';
$route['member/edit/(:any)'] = 'MemberController/edit/$1';
$route['member/update/(:any)'] = 'MemberController/update/$1';
$route['member/delete/(:any)'] = 'MemberController/delete/$1';
$route['member/get'] = 'MemberController/get';

// Route Kategori
$route['kategori'] = 'KategoriController';
$route['kategori/create'] = 'KategoriController/create';
$route['kategori/store'] = 'KategoriController/store';
$route['kategori/(:any)'] = 'KategoriController/show/$1';
$route['kategori/edit/(:any)'] = 'KategoriController/edit/$1';
$route['kategori/update/(:any)'] = 'KategoriController/update/$1';
$route['kategori/delete/(:any)'] = 'KategoriController/delete/$1';

$route['subkategori'] = 'SubKategoriController';
$route['subkategori/create/(:any)'] = 'SubKategoriController/create/$1';
$route['subkategori/store'] = 'SubKategoriController/store';
$route['subkategori/edit/(:any)'] = 'SubKategoriController/edit/$1';
$route['subkategori/update/(:any)'] = 'SubKategoriController/update/$1';
$route['subkategori/delete/(:any)'] = 'SubKategoriController/delete/$1';

// Route Supplier
$route['supplier'] = 'SupplierController';
$route['supplier/create'] = 'SupplierController/create';
$route['supplier/store'] = 'SupplierController/store';
$route['supplier/edit/(:any)'] = 'SupplierController/edit/$1';
$route['supplier/update/(:any)'] = 'SupplierController/update/$1';
$route['supplier/delete/(:any)'] = 'SupplierController/delete/$1';

// Route Satuan
$route['satuan'] = 'SatuanController';
$route['satuan/create'] = 'SatuanController/create';
$route['satuan/store'] = 'SatuanController/store';
$route['satuan/edit/(:any)'] = 'SatuanController/edit/$1';
$route['satuan/update/(:any)'] = 'SatuanController/update/$1';
$route['satuan/delete/(:any)'] = 'SatuanController/delete/$1';

// Route Product
$route['product'] = 'ProductController';
$route['product/create'] = 'ProductController/create';
$route['product/store'] = 'ProductController/store';
$route['product/show/(:any)'] = 'ProductController/show/$1';
$route['product/edit/(:any)'] = 'ProductController/edit/$1';
$route['product/update/(:any)'] = 'ProductController/update/$1';
$route['product/delete/(:any)'] = 'ProductController/delete/$1';
$route['product/get'] = 'ProductController/get';
$route['product/get-kode'] = 'ProductController/getKode';

// Route Stock
$route['stock'] = 'StockController';
$route['stock/create'] = 'StockController/create';
$route['stock/store'] = 'StockController/store';
$route['stock/edit/(:any)'] = 'StockController/edit/$1';
$route['stock/update/(:any)'] = 'StockController/update/$1';
$route['stock/delete/(:any)'] = 'StockController/delete/$1';

// Route lost
$route['lost-product'] = 'LostProductController';
$route['lost-product/create'] = 'LostProductController/create';
$route['lost-product/store'] = 'LostProductController/store';
$route['lost-product/edit/(:any)'] = 'LostProductController/edit/$1';
$route['lost-product/update/(:any)'] = 'LostProductController/update/$1';
$route['lost-product/delete/(:any)'] = 'LostProductController/delete/$1';

// Route Pembelian
$route['pembelian'] = 'PembelianController';
$route['pembelian/create'] = 'PembelianController/create';
$route['pembelian/store'] = 'PembelianController/store';
$route['pembelian/edit/(:any)'] = 'PembelianController/edit/$1';
$route['pembelian/update/(:any)'] = 'PembelianController/update/$1';
$route['pembelian/delete/(:any)'] = 'PembelianController/delete/$1';

// Route Transaksi
$route['transaksi'] = 'TransaksiController';
$route['transaksi/create'] = 'TransaksiController/create';
$route['transaksi/store'] = 'TransaksiController/store';
$route['transaksi/show/(:any)'] = 'TransaksiController/show/$1';
$route['transaksi/print/(:any)'] = 'TransaksiController/print/$1';
$route['transaksi/edit/(:any)'] = 'TransaksiController/edit/$1';
$route['transaksi/update/(:any)'] = 'TransaksiController/update/$1';
$route['transaksi/delete/(:any)'] = 'TransaksiController/delete/$1';
$route['transaksi/counted'] = 'TransaksiController/counted';

// Route Detail Transaksi
$route['detail-transaksi/(:num)'] = 'DetailTransaksiController/get/$1';
$route['detail-transaksi/store'] = 'DetailTransaksiController/store';
$route['detail-transaksi/update'] = 'DetailTransaksiController/update';
$route['detail-transaksi/delete'] = 'DetailTransaksiController/delete';

// Route Transaksi
$route['piutang'] = 'PiutangController';
$route['piutang/create'] = 'PiutangController/create';
$route['piutang/store'] = 'PiutangController/store';
$route['piutang/show/(:any)'] = 'PiutangController/show/$1';
$route['piutang/print/(:any)'] = 'PiutangController/print/$1';
$route['piutang/edit/(:any)'] = 'PiutangController/edit/$1';
$route['piutang/update/(:any)'] = 'PiutangController/update/$1';
$route['piutang/delete/(:any)'] = 'PiutangController/delete/$1';

// Route Laporan
$route['laporan/transaksi'] = 'TransaksiController/laporan';
$route['transaksi/export'] = 'TransaksiController/export';
$route['laporan/piutang'] = 'PiutangController/laporan';
$route['piutang/export'] = 'PiutangController/export';
$route['laporan/penjualan'] = 'ProductController/laporan';
$route['penjualan/export'] = 'ProductController/export';

// Route Setting
$route['setting'] = 'DashboardController/setting';
$route['setting/update'] = 'DashboardController/updatesetting';
$route['profile'] = 'DashboardController/profile';
$route['profile/update'] = 'DashboardController/updateprofile';
$route['sync'] = 'DashboardController/sync';

// Route List Member
$route['list-member-bulanan'] = 'ListMemberController/bulanan';
$route['list-member-tahunan'] = 'ListMemberController/tahunan';
